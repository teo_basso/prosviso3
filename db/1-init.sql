CREATE EXTENSION dblink;

DO
$do$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = 'teamorganizeruser') THEN
    CREATE ROLE teamorganizeruser LOGIN PASSWORD 'teamorganizerpassword';
  END IF;

  IF NOT EXISTS (SELECT 1 FROM pg_database WHERE datname = 'teamorganizer') THEN
    PERFORM dblink_exec('dbname=' || current_database()
                      , 'CREATE DATABASE teamorganizer');
  END IF;

  GRANT ALL PRIVILEGES ON DATABASE teamorganizer TO teamorganizeruser;
END
$do$;
