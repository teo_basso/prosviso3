# Team Organizer
Terzo Assignment di Processo e Sviluppo del Software.

Basso Matteo 807628
Repository GitLab: [https://gitlab.com/teo_basso/prosviso3.git](https://gitlab.com/teo_basso/prosviso3.git)

# Indice dei contenuti

[TOC]

# Introduzione

L'obiettivo del seguente documento è quello di esplicare il domio dell'applicazione sviluppata, riportare le strategie utilizzate per il suo sviluppo ed evidenziare l'architettura utilizzata per implementare le varie funzionalità nella maniera più modulare e manutenibile possibile.

Il seguente elaborato e lo sviluppo dell'applicazione sono stati svolti da Basso Matteo, matricola 807628, e il link al repository GitLab contenente il codice sorgente risulta il seguente: https://gitlab.com/teo_basso/prosviso3.git. In particolare vengono sviluppate sette entità, tre relazioni molti a uno, una self relation, e una relazione molti a molti con lazy load. Tre di queste entità inoltre risultano coinvolte in una relazione di ereditarietà. Sono presenti funzioni per la ricerca di tali entità e viene sviluppata inoltre una semplice UI che permette di effettuare le tipiche operazioni CRUD su ognuna di esse.

# Dominio dell'applicazione

L'applicazione consente di pianificare i turni di lavoro dei dipendenti di una software house fornendo all'utilizzatore la possibilità di definire orari, progetto interessato alla sviluppo e dipendenti coinvolti.

In particolare l'utente è in grado di definire, preventivamente alla programmazione dei turni:

- *Ufficio*: ufficio presso il quale viene svolta l'attività lavorativa, è costituito dalle informazioni geografiche che lo caratterizzano, quali stato, città, indirizzo etc.
- *Progetto*: rappresenta un progetto che deve essere sviluppato dell'azienda, caratterizzato da un nome univoco, una versione e l'*Ufficio* che lo ha preso in carico.
- *Dipendente*:  costituisce un qualsiasi dipendente dell'azienda.
- *Sviluppatore*: costituisce un particolare tipo di dipendente, ovvero uno sviluppatore, in grado di sviluppare i progetti. Ad esso sono associati vari ruoli.
- *Ruolo*: rappresenta una mansione che può essere svolta da uno sviluppatore. Per esempio lo sviluppo in diversi linguaggi di programmazione.
- *Impiegato*: costituisce un particolare tipo di dipendente, ovvero un impiegato, che svolge attività amministrative relative ai progetti e non. Viene inoltre indicato un altro dipendente come suo manager.

Dopo aver definito tali informazioni l'utente è in grado di programmare le attività per ogni dipendente specificando:

- Data e ora di inizio
- Data e ora di finire
- Dipendente (dipendente generico, operaio o impiegato)
- Eventuale progetto su cui viene svolta la lavorazione

# Installazione ed esecuzione

## Prerequisiti

- [npm (optional)](http://npmjs.com/)
- [java](https://www.java.com)
- [mvn](https://maven.apache.org/)
- [docker](https://www.docker.com/)

Per validare l'installazione è necessario eseguire i seguenti comandi:

```
npm -v
java -version
mvn -v
docker -v
```

## Esecuzione

Clonare, installare le dipendenze ed eseguire la run:

```bash
git clone https://github.com/teo_basso/prosviso3.git
cd prosviso3
npm install # or: cd app && mvn install -DskipTests && cd ..
npm start # or: npm run dev:down && npm run dev:install && npm run dev:up
```

Ora è possibile aprire `http://localhost:8888` per vedere l'applicazione.

Si noti che nel caso in cui `npm start` mostri alcuni errori, potrebbe essere a causa di un bug di docker, è dunque necessario riavviare docker stesso e lanciare nuovamente il comando.

## Test

Per lanciare i test eseguire:

```bash
npm test # or: cd docker && docker-compose run --rm app mvn test && cd ..
```

Questi test sono inoltre eseguiti automaticamente dalla CI di GitLab, [qui](https://gitlab.com/teo_basso/prosviso3/pipelines) sono presenti le pipeline.

## Build di produzione

```bash
npm run prod:install # or: docker-compose -f docker/docker-compose.prod.yml build
```

Questo creerà tutte le immagini docker definiti all'interno del file  `docker/docker-compose.prod.yml`. Risulta dunque possibile eseguire il deploy dei container usando, per esempio, un servizio cloud.

## Altri script

Nel file `package.json` sono definiti altri script utili per fare la build, eseguire e distruggere le immagini docker. Ogni script ha un prefisso relativo il suo ambiente di esecuzione. Per esempio gli script `dev:..` sono quelli relativi all'ambiente di *dev* della configurazione docker. Essi possono essere eseguiti, per esempio, come segue:

```bash
npm run dev:uninstall
```

# Analisi tecnica

Di seguito sono elencati i dettagli implementativi dei vari aspetti del sistema.

## DevOps

Vengono di seguito illustrati gli strumenti e/o le strategie di DevOps utilizzate durante lo svolgimento dell'assignment.

### GitFlow

Il repository GitLab risulta strutturato secondo il branching model [GitFlow](https://datasift.github.io/gitflow/IntroducingGitFlow.html). Esso permette di avere diversi branch dedicati a diverse feature, fix, o release dell'applicazione in modo tale da parallelizzare gli sviluppi, favorire la collaborazione, permettere la manutenzione di una staging area e supportare fix d'emergenza.

Esso è stato preferito rispetto al [GitHub Fow](https://guides.github.com/introduction/flow/) ponendo come assunzione un elevato livello di complessità dell'applicativo e numerosità delle persone appartenenti al team di sviluppo.

### Containerization

Per separare al meglio l'applicazione dal database, facilitare gli sviluppi, il processo di build e deploy, è stato impiegato [docker](https://www.docker.com/) per la creazione di container. In particolare vengono creati due container, uno per l'applicazione Java e uno per il database.

Nello specifico sono presenti all'interno del progetto tre configurazioni differenti:

- *dev*: configurazione di sviluppo, rappresenta quella che viene eseguita sulla macchina locale. Il filesystem della macchina locale mappa quello del container Java, che risulta connesso con quello del database.
- *test*: configurazione di test che viene eseguita quando si lancia il comando per eseguire i test di JUnit. Essa risulta utile poichè utilizza un volume differente del database rispetto a quello di *dev*, questo consente di avere sempre a disposizione un database pulito e non modificare quello che lo sviluppatore potrebbe utilizzare mediante il browser.
- *prod*: configurazione di produzione, le immagini vengono qui create effettuando la build del sistema e copiando all'interno del container finale l'applicazione compilata. Vengono dunque differenziati anche i comandi per eseguirla una volta che si effettua il deploy.

Per effettuare ciò è presente una cartella `docker` contenete nella root i file di compose necessari per effettuare le varie operazioni di build, up, down etc. Sono inolte presenti 2 ulteriori sottocartelle `app` e `db` che contengono invece i `Dockerfile` necessari per creare le immagini docker. Mentre nella cartella `db` è presente un solo `Dockerfile` utilizzato sia per *dev* che per *prod*, nella cartella `app` possiamo trovarne due differenti che svolgono quanto esplicato precedentemente.

### Continuous integration

All'interno del repository è presente un file di configurazione `.gitlab-ci.yml` utile per il setup dell'ambiente di Continuous integration. Esso viene eseguito in un container che espone a sua volta docker ed è costituito da 3 stage:

- *Install*: in questo stage vengono installate la dipendenze e viene installato ed eseguito l'ambiente di sviluppo, così da verificare che tutto funzioni al meglio.
- *test*: vengono eseguiti i test definiti nell'applicazione Java.
- *build*: viene effettuata la build di produzione ed eseguita nella CI, per verificare che tutto funzioni correttamente e risulti rilasciabile.

Tramite questi 3 stage è quindi possibile verificare il corretto funzionamento di tutte le configurazioni docker presenti e rilevare immediatamente eventuali regressioni o errori di build.

## Database

Per quanto riguarda il database viene utilizzato [PostgreSQL](https://www.postgresql.org/) tramite un'apposita immagine docker. L'unica configurazione che è stato necessario inserire riguarda la creazione di un utente e di un database dedicato all'applicazione in questione, tale script, situato nella cartella `db` viene compiato nella directory di inizializzazione `init.d` del container, così da far sì che venga automaticamente eseguito dal database. All'interno dello script vengono posti opportuni controlli per verificare eventualmente la pre-esistenza dell'utente e del database in questione.

Si noti che non sono presenti script di creazione delle varie tabelle, poichè verrano create automaticamente da hibernate durante l'esecuzione dell'applicativo Java. La struttura del database viene quindi definita dai Model e rispetta quanto riportato nel capitolo [Dominio dell'applicazione](#dominio-dell-applicazione). Di seguito è riportato il diagramma ER del database autogenerato da hibernate ed esportato mediante il plugin [Jeddict](https://jeddict.github.io/index.html), esplicato nella sezione riguardante l'applicazione Java:

![Er Diagram](.\images\er_diagram.png)

Nei modelli Java, per scelta architetturale, non sono presenti sempre relazioni bidirezionali da e verso le entità interessate. Nel caso degli sviluppatori e dei ruoli per esempio, è presente un campo `roles` all'interno dell'entità `Developer` ma non è presente un campo `developers` all'interno dell'entità `Role`.

Per quanto riguarda i Cascade, vengono identificate le seguenti regole, che portano poi a delle specifiche annotazioni degli attributi nella classi Java:

- Alla cancellazione di un ufficio devono essere cancellati i progetti associati, in quanto non più di competenza dell'azienda. Nel caso in cui si vogliano passare le commesse è necessario aggiornare i progetti e successivamente cancellare l'ufficio interessato.
- Alla cancellazione di un dipendente, impiegato o sviluppatore, così come alla cancellazione di un progetto, devono essere cancellate tutte le schedulazioni associate, in quanto non è più possibile svolgerle per mancanza di personale o oggetto di lavoro. Va quindi ripianificata l'attività.
- La cancellazione di un manager non può essere svolta se non vengono prima riallocati i suoi sottoposti. Ciò significa che prima di essere licenziato, egli deve formare e riorganizzare l'organigramma aziendale.
- La cancellazione di un ruolo non può essere svolta se non vengono prima eliminati dai rispettivi dipendenti. Ciò rappresenta un vincolo per far sì che si proceda con cautela nell'assegnazione e modifica delle mansioni degli sviluppatori.

## Java Application

Di seguito vengono indicate le spefiche implementative dell'applicazione Java, che risulta sviluppata con l'utilizzo del framework [Spring](https://spring.io/) e dell'ORM [hibernate](http://hibernate.org/). Il codice sorgente relativo questa sezione risiede interamente all'interno della directory `app`.

### Gestione del progetto e delle dipendenze

Per gestire il progetto e le dipendenze e stato utilizzato [maven](https://maven.apache.org/). L'installazione, l'esecuzione dei test e la build vengono dunque effettuate utilizzando i suoi comandi.

### WebServer e boot

Per creare il progetto è stato utilizzato [spring-boot](http://spring.io/projects/spring-boot), un tool che ha permesso la creazione dell'ambiente in modo veloce e intuitivo, senza doversi preoccupare della creazione e configurazione della struttura dei sorgenti e del WebServer.

### Architettura

All'interno della cartella `src/main/java`, e successivamente all'interno del package, sono presenti i file del codice sorgente dell'applicazione. Di seguito vengono indicati i file e le directory presenti, specificando per ognuno di essi la propria funzione. Di utilità generale risultano i seguenti:

- `Application.java`: contiene una classe che rappresenta l'entrypoint dell'app, essa viene decorata con le opportune annotazioni fornite da spring per avviare il framework nel modo opportuno. Viene quindi chiamata la funzione di run all'interno del metodo `main`.
- `Diagram.jpa`: rappresenta un file autogenerato dal plugin [Jeddict](https://jeddict.github.io/index.html) per [NetBeans](https://netbeans.org/) a partire dal database dell'applicazione. Esso permette di visualizzare il diagramma ER generato da hibernate, così come le entità. Mediante il suo utilizzo è anche possibile generare il codice sorgente a seguito della creazione di un diagramma UML da parte dell'utente. Quest'ultima funzionalità tuttavia non è stata utilizzata, tale plugin risulta quindi impiegato solamente per la visualizzazione dei diagramma e dunque la loro esportazione in immagine da includere in questo documento.

Viene ora effettuata una distinzione tra la parte di accesso ai dati e web app.

#### Data access

Le classi relative l'accesso ai dati sono definite all'interno del package dell'applicazione come segue:

- `model`: nella cartella model sono presenti i modelli utilizzati dall'applicazione, ovvero le classi java che rappresentano gli oggetti di dominio e vengono decorate con le annotazioni di hibernate per poter effettuare le operazioni sul database relazionale.
- `repository`: nella cartella repository sono presenti, appunto, i repository relativi ai vari aggregati, in questo caso uno per ciascun modello. L'applicazione implementa dunque il [Repository Pattern](https://docs.microsoft.com/en-us/dotnet/standard/microservices-architecture/microservice-ddd-cqrs-patterns/infrastructure-persistence-layer-design). Esso permette di creare delle classi che incapsulano la logica richiesta per accedere alla fonte dati, fornendo una maggiore manutenibilità e disaccoppiamento dell'infrastrattura utilizzata per accedere al database dal livello di dominio. Questo permette di ottenere vari benefici, nascondendo l'infrastruttura e i sistemi di persistenza dei dati all'utilizzatore del repository è possibile cambiare modalità di storage in modo totalmente trasparente. Dal punto di vista del testing quindi, il repository permette di testare unicamente l'astrazione del codice. Risulta possibile per esempio fornire, in fase di test , tramite Dependency Injection, repository mock che restituiscono dei dati fake e permettono quindi di verificare il corretto funzionamento del sistema senza avere a disposizione il database relazionale.
  Nel caso in esame, ogni repository è costituito da due file (eccetto quelli relativi alle entità che ereditano da un model comune che sono costituiti da tre). Prendendo per esempio il model `Project` possiamo identificare i seguenti file: `ProjectRepository` e `ProjectRepositoryCustom`. Il file `ProjectRepositoryCustom` definisce un'interfaccia contenente una serie di metodi specifici relativi al modello per effettuare operazioni di search. Il file `ProjectRepository` definisce invece un'ulteriore interfaccia che estende quella appena descritta e `JpaRepository<Model, Id>`, viene inoltre decorata con l'annotazione `@Repository` in modo tale che spring sia in grado di identificarla automaticamente e utilizzarla per la Dependency Injection. Tale definizione quindi fa sì che il repository contenga tutti i metodi per effettuare le operazioni CRUD per il modello indicato, unitamente a quelle definite custom.

#### Web / UI

L'interfaccia utente risulta sviluppata mediante il framework [struts2](https://struts.apache.org/) di apache. Esso viene integrato all'interno di spring-boot, così da poter usufruire della Dependency Injection di Spring e utilizzare i Repository precedentemente definiti. Ogni entità risulta modificabile mediante un'apposita pagina tramite le tipiche operazioni CRUD. Non sono presenti al momento all'interno della UI le funzioni di ricerca.

Per quanto riguarda gli stili CSS è stata importata la libreria [Materialize CSS](https://materializecss.com/), una serie di stili, script e componenti utili per creare un'interfaccia grafica che segua le linee guida di [Material design](https://material.io/design/), il famoso framework visuale disegnato da Google.

Vengono qui definiti i file utilizzati all'esterno del package:

- `resource/struts.xml`: file di configurazione di struts, vengono definite al suo interno le action dell'applicazione, nello specifico 4 per ogni entità (create, read, update e delete) e una pagina di benvenuto. Viene inoltre impostata una pagina di errore generico nel caso in cui l'applicazione dovesse scatenare un'eccezione. Viene inoltre impostato il tema `simple` e una configuazione per la Dependency Injection di Spring.
- `webapp`: contiene i file `.jsp` ovvero le view dell'applicazione, al suo interno sono presenti:
  - `WEB-INF/tags/page.tag`: rappresenta il template condiviso da tutte le pagine dell'applicazione. Fornisce la barra della applicazioni nella parte superiore dello schermo, il menu laterale per la navigazione, gli stili e alcuni script per inizializzare correttamente i componenti di materialize CSS.
  - `components`: contiene una serie di file inclusi in molteplici view, che per questo motivo vengono risposti in una cartella chiamata *componenti*.
  - `pages`: sono riposte al suo interno le vere e proprie pagine dell'applicazione che utilizzano il template precedentemente descritto. Esse risultano quindi la pagina di benvenuto, di errore generico e una per ogni entità.

All'interno del package sono presenti poi differenti cartelle e file qui descritti:

- `config`: all'interno di questa cartella vengono riposte le classi di configurazione dell'applicazione. Nel caso in esame risulta presente solamente una classe utile alla configurazione di struts, in particolare risulta necessario impostare dei filtri per le richieste così da poterle gestire nel modo corretto.
- `action`: contiene le azioni struts che vengono invocate e forniscono i dati alle view. Al suo interno è presente una classe generica `Action` che viene poi estesa da quelle più specifiche per ogni entità. Ciò permette di riutilizzare gran parte del codice che andrebbe altrimenti duplicato e fornire un flusso di funzionamento standard. In particolare tale classe è in grado di utilizzare un service per gestire un modello e una lista dello stesso.
- `service`:  sono presenti al suo interno i servizi utilizzati dalle azioni. Essi permettono di incapsulare i repository per l'accesso ai dati. Al suo interno è presente un'interfaccia generica `ServiceFacade` e la sua implementazione `ServiceFacadeImpl`, tutti i repository dunque estendono quest'ultima per aderire a un'inferfaccia comune.

### Test

Nella cartella `src/test`, in corrispondenza del package dell'applicazione, sono presenti vari test divisi in base al file a cui si riferiscono. Viene dunque mantenuta la stessa struttura della directory del codice sorgente.

Nella cartella `repository` vengono riposti i test dei vari repository, utili anche a testare i Model ad essi associati. Vengono qui definiti test per verificare il corretto inserimento, aggiornamento, cancellazione e ovviamente lettura dei dati. Vengono testati inoltre i metodi custom dei vari repository, le eccezioni che devo essere sollevate nel caso in cui si cerchi di inserire dati che violano le regole di dominio, ed infine le relazioni tra i vari modelli.

Nella cartella `model` sono presenti i test della classe astratta `Audit`. Essi utilizzano le classi concrete presenti nella cartella `mock`, non sarebbe stato altrimenti possibile testare le sue funzionalità.

Viene inoltre riportato un test al di fuori delle varie cartelle per verificare il corretto avvio dell'applicazione.

Si noti che essi devono essere eseguiti all'interno del container docker, ovvero lanciando l'apposito comando delineato nei capitoli successivi, e non eseguibili normalmente tramite l'IDE. Ciò risulta necessario poichè essi sono dipendenti dal database ed è dunque necessario installare un ulteriore container. Come già specificato, essi vengono eseguiti automaticamente dalla Continuous integration.

Non sono presenti al momento test per la UI, un possibile miglioramento sotto questo punto di vista potrebbe essere quello di utilizzare strumenti quali, per esempio, [Selenium](https://www.seleniumhq.org/), per verificare il corretto funzionamento del sistema ed identificare eventuali regressioni.
