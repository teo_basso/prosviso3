# Team Organizer

## Prerequisites

- [npm (optional)](http://npmjs.com/)
- [java](https://www.java.com)
- [mvn](https://maven.apache.org/)
- [docker](https://www.docker.com/)

To validate the installation, please run the following commands:

```bash
npm -v
java -version
mvn -v
docker -v
```

## Getting started

Clone, install dependencies and run:

```bash
git clone https://github.com/teo_basso/prosviso3.git
cd prosviso3
npm install # or: cd app && mvn install -DskipTests && cd ..
npm start # or: npm run dev:down && npm run dev:install && npm run dev:up
```

Then open `http://localhost:8888` to see the app.

Please note that if `npm start` gives you some errors, it might due to a docker bug, so it might be necessary to restart docker itself and run the command again.

## Tests

To run the test suite just run:

```bash
npm test # or: cd docker && docker-compose run --rm app mvn test && cd ..
```

These tests are also runned automatically by GitLab CI, [here](https://gitlab.com/teo_basso/prosviso3/pipelines) are the pipelines.

## Building for Production

```bash
npm run prod:install # or: docker-compose -f docker/docker-compose.prod.yml build
```

This will create all the dockers containers defined in the `docker/docker-compose.prod.yml` file. It is now possible to deploy these containers using a cloud service.

## Other scripts

In the `package.json` there are some other useful scripts to build, run and destroy docker images. Each script has its own environment prefix. For example the `dev:..` scripts work using the docker dev configuration. Scripts can be runned, for example, in this way:

```bash
npm run dev:uninstall
```
