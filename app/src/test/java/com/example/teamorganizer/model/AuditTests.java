package com.example.teamorganizer.model;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.example.teamorganizer.mock.AuditImpl;
import com.example.teamorganizer.mock.AuditImplRepository;
import com.example.teamorganizer.model.Audit;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class AuditTests {
  @Autowired
  private AuditImplRepository auditImplRepository;

  @Before
  public void beforeEach() {
    auditImplRepository.deleteAll();
  }

  @Test
  public void shouldBeAnAudit() {
    Assert.assertTrue(Audit.class.isAssignableFrom(AuditImpl.class));
  }

  @Test
  public void shouldCreateNewAudits() {
    AuditImpl audit = new AuditImpl();
    Assert.assertNull(audit.getId());
    Assert.assertNull(audit.getCreatedAt());
    Assert.assertNull(audit.getUpdatedAt());
    auditImplRepository.saveAndFlush(audit);

    Assert.assertNotNull(audit.getId());
    Assert.assertNotNull(audit.getCreatedAt());
    Assert.assertNotNull(audit.getUpdatedAt());
    Assert.assertNotNull(auditImplRepository.getOne(audit.getId()));
  }

  @Test
  public void shouldUpdateAnAudit() throws InterruptedException  {
    AuditImpl audit = new AuditImpl();
    audit.setValue("val1");

    Assert.assertNull(audit.getId());
    Assert.assertNull(audit.getCreatedAt());
    Assert.assertNull(audit.getUpdatedAt());
    auditImplRepository.saveAndFlush(audit);

    Date createdAt = audit.getCreatedAt();
    Date updatedAt = audit.getUpdatedAt();

    Assert.assertNotNull(audit.getId());
    Assert.assertNotNull(createdAt);
    Assert.assertNotNull(updatedAt);
    Assert.assertNotNull(auditImplRepository.getOne(audit.getId()));

    TimeUnit.MILLISECONDS.sleep(1000);

    audit.setValue("val2");
    auditImplRepository.saveAndFlush(audit);

    Assert.assertNotNull(audit.getId());
    Assert.assertNotNull(audit.getCreatedAt());
    Assert.assertNotNull(audit.getUpdatedAt());
    Assert.assertNotNull(auditImplRepository.getOne(audit.getId()));
    Assert.assertEquals(createdAt, audit.getCreatedAt());
    Assert.assertNotEquals(updatedAt, audit.getUpdatedAt());
  }
}
