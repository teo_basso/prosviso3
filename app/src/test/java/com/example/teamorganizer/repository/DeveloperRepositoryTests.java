package com.example.teamorganizer.repository;

import com.example.teamorganizer.model.Developer;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import com.example.teamorganizer.model.Audit;
import com.example.teamorganizer.model.Employee;
import com.example.teamorganizer.model.Role;
import com.example.teamorganizer.repository.DeveloperRepository;
import com.example.teamorganizer.repository.RoleRepository;

import org.hibernate.collection.internal.PersistentSet;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class DeveloperRepositoryTests {
  String name = "Matteo";
  String surname = "Basso";
  String serialNumber = "807628";
  Role role = new Role();
  HashSet<Role> roles = new HashSet<Role>(Arrays.asList(role));

  @Autowired
  private DeveloperRepository developerRepository;

  @Autowired
  private EmployeeRepository employeeRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Before
  public void beforeEach() {
    developerRepository.deleteAll();
    roleRepository.deleteAll();
    developerRepository.flush();
    roleRepository.flush();

    role.setName("PYTHON_DEVELOPER");
    role.setDescription("python junior developer");
  }

  @Test
  public void shouldBeAnAudit() {
    Assert.assertTrue(Audit.class.isAssignableFrom(Developer.class));
  }

  @Test
  public void shouldBeAnEmployee() {
    Assert.assertTrue(Employee.class.isAssignableFrom(Developer.class));
  }

  @Test
  public void shouldCreateNewWorkers() {
    Role newRole = new Role();
    newRole.setName("SUPERVISOR");

    HashSet<Role> developerRoles = new HashSet<Role>(Arrays.asList(role, newRole));

    Developer developer = new Developer();
    developer.setSerialNumber(serialNumber);
    developer.setName(name);
    developer.setSurname(surname);
    developer.setRoles(developerRoles);
    Assert.assertNull(developer.getId());
    developerRepository.saveAndFlush(developer);

    Assert.assertEquals(serialNumber, developer.getSerialNumber());
    Assert.assertEquals(name, developer.getName());
    Assert.assertEquals(surname, developer.getSurname());
    Assert.assertEquals(developerRoles, developer.getRoles());
    Assert.assertEquals(2, developerRoles.size());
    Assert.assertTrue(developerRoles.contains(role));
    Assert.assertTrue(developerRoles.contains(newRole));
    Assert.assertNotNull(developer.getId());
    Assert.assertNotNull(developerRepository.getOne(developer.getId()));
  }

  @Test(expected = JpaObjectRetrievalFailureException.class)
  public void shouldDeleteAWorker() {
    Developer developer = new Developer();
    developer.setSerialNumber(serialNumber);
    developer.setName(name);
    developer.setSurname(surname);
    developer.setRoles(roles);
    developerRepository.saveAndFlush(developer);
    developerRepository.delete(developer);
    developerRepository.getOne(developer.getId());
  }

  @Test
  public void shouldUpdateAWorker() {
    String newName = "Matteo";
    String newSurname = "Basso";
    String newSerialNumber = "807628";
    Role newRole = new Role();
    newRole.setName("SUPERVISOR");

    roleRepository.saveAndFlush(newRole);

    HashSet<Role> newRoles = new HashSet<Role>(Arrays.asList(newRole));

    Developer developer = new Developer();
    developer.setSerialNumber(serialNumber);
    developer.setName(name);
    developer.setSurname(surname);
    developer.setRoles(roles);
    developerRepository.saveAndFlush(developer);

    developer.setSerialNumber(newSerialNumber);
    developer.setName(newName);
    developer.setSurname(newSurname);
    developer.setRoles(newRoles);
    developerRepository.saveAndFlush(developer);

    Developer updatedWorker = developerRepository.getOne(developer.getId());
    Assert.assertEquals(newSerialNumber, updatedWorker.getSerialNumber());
    Assert.assertEquals(newName, updatedWorker.getName());
    Assert.assertEquals(newSurname, updatedWorker.getSurname());
    Assert.assertEquals(developer.getId(), updatedWorker.getId());

    PersistentSet rolesSet = (PersistentSet) updatedWorker.getRoles();
    Object[] updatedRoles = rolesSet.toArray(new Role[rolesSet.size()]);

    Assert.assertEquals(1, updatedRoles.length);
    Assert.assertEquals(newRole.getId(), ((Role) updatedRoles[0]).getId());
  }

  @Test
  public void shouldUpdateAnEmployee() {
    String newSerialNumber = "000999";
    Developer developer = new Developer();
    developer.setSerialNumber(serialNumber);
    developer.setName(name);
    developer.setSurname(surname);
    developer.setRoles(roles);
    developerRepository.saveAndFlush(developer);

    Employee employee = employeeRepository.getOne(developer.getId());
    employee.setSerialNumber(newSerialNumber);
    Employee updatedEmployee = employeeRepository.saveAndFlush(employee);

    Assert.assertEquals(developer.getId(), updatedEmployee.getId());
    Assert.assertEquals(newSerialNumber, updatedEmployee.getSerialNumber());
  }

  @Test
  public void shouldCreateANewRole() {
    Assert.assertNull(role.getId());

    Developer developer = new Developer();
    developer.setSerialNumber(serialNumber);
    developer.setName(name);
    developer.setSurname(name);
    developer.setRoles(roles);
    developerRepository.saveAndFlush(developer);

    Assert.assertNotNull(developer.getId());
    Assert.assertNotNull(developerRepository.getOne(developer.getId()));
    Assert.assertNotNull(roleRepository.getOne(role.getId()));
  }

  @Test
  public void shouldUseAnExistingRole() {
    Assert.assertNull(role.getId());

    roleRepository.saveAndFlush(role);

    Long roleId = role.getId();
    Assert.assertNotNull(roleId);

    Developer developer = new Developer();
    developer.setSerialNumber(serialNumber);
    developer.setName(name);
    developer.setSurname(name);
    developer.setRoles(roles);
    developerRepository.saveAndFlush(developer);

    Assert.assertNotNull(developer.getId());
    Assert.assertNotNull(developerRepository.getOne(developer.getId()));
    Assert.assertNotNull(roleRepository.getOne(role.getId()));
    Assert.assertEquals(roleId, role.getId());
  }

  @Test
  public void shouldNotDeleteARole() {
    Assert.assertNull(role.getId());

    roleRepository.saveAndFlush(role);

    Long roleId = role.getId();
    Assert.assertNotNull(roleId);

    Developer developer = new Developer();
    developer.setSerialNumber(serialNumber);
    developer.setName(name);
    developer.setSurname(name);
    developer.setRoles(roles);
    developerRepository.saveAndFlush(developer);
    developerRepository.delete(developer);

    Assert.assertNotNull(roleRepository.getOne(role.getId()));
    Assert.assertEquals(roleId, role.getId());
  }

  @Test
  public void shouldUpdateARole() {
    String newName = "PYTHON_DEVELOPER";

    Role newRole = new Role();
    newRole.setName("SUPERVISOR");

    roleRepository.saveAndFlush(newRole);

    Long roleId = newRole.getId();
    Assert.assertNotNull(roleId);

    Developer developer = new Developer();
    developer.setSerialNumber(serialNumber);
    developer.setName(name);
    developer.setSurname(name);
    developer.setRoles(new HashSet<Role>(Arrays.asList(newRole)));
    developerRepository.saveAndFlush(developer);

    newRole.setName(newName);

    PersistentSet rolesSet = (PersistentSet) developerRepository.saveAndFlush(developer).getRoles();
    Object[] updatedRoles = rolesSet.toArray(new Role[rolesSet.size()]);
    Role updatedRole = (Role) updatedRoles[0];

    Assert.assertNotNull(updatedRoles);
    Assert.assertEquals(1, updatedRoles.length);
    Assert.assertEquals(roleId, updatedRole.getId());
    Assert.assertEquals(newName, updatedRole.getName());
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldThrowsIfTriesToDeleteARoleAssociatedToADeveloper() {
    Developer developer = new Developer();
    developer.setSerialNumber(serialNumber);
    developer.setName(name);
    developer.setSurname(surname);
    developer.setRoles(roles);
    developerRepository.saveAndFlush(developer);

    roleRepository.deleteAllInBatch();
  }

  @Test
  public void shouldExtendEmployeeCustomRepository() {
    Assert.assertTrue(developerRepository instanceof EmployeeRepositoryCustom<?>);
    Optional<Developer> developer = developerRepository.findBySerialNumber(serialNumber);
  }

  @Test
  public void shouldFindWorkersByRole() {
    Role newRole = new Role();
    newRole.setName("SUPERVISOR");

    Developer developer1 = new Developer();
    developer1.setSerialNumber(serialNumber);
    developer1.setName(name);
    developer1.setSurname(surname);
    developer1.setRoles(roles);
    developerRepository.saveAndFlush(developer1);

    Developer developer2 = new Developer();
    developer2.setSerialNumber("111111");
    developer2.setName(name);
    developer2.setSurname(surname);
    developer2.setRoles(new HashSet<Role>(Arrays.asList(newRole)));
    developerRepository.saveAndFlush(developer2);

    Developer developer3 = new Developer();
    developer3.setSerialNumber("222222");
    developer3.setName(name);
    developer3.setSurname(surname);
    developer3.setRoles(roles);
    developerRepository.saveAndFlush(developer3);

    List<Developer> developers = developerRepository.findByRoles(role);
    Assert.assertEquals(2, developers.size());
    Assert.assertEquals(developer1, developers.get(0));
    Assert.assertEquals(developer3, developers.get(1));
  }

  @Test
  public void shouldFindWorkersByRoleName() {
    Role newRole = new Role();
    newRole.setName("SUPERVISOR");

    Developer developer1 = new Developer();
    developer1.setSerialNumber(serialNumber);
    developer1.setName(name);
    developer1.setSurname(surname);
    developer1.setRoles(roles);
    developerRepository.saveAndFlush(developer1);

    Developer developer2 = new Developer();
    developer2.setSerialNumber("111111");
    developer2.setName(name);
    developer2.setSurname(surname);
    developer2.setRoles(new HashSet<Role>(Arrays.asList(newRole)));
    developerRepository.saveAndFlush(developer2);

    Developer developer3 = new Developer();
    developer3.setSerialNumber("222222");
    developer3.setName(name);
    developer3.setSurname(surname);
    developer3.setRoles(roles);
    developerRepository.saveAndFlush(developer3);

    List<Developer> developers = developerRepository.findByRoles_Name(role.getName());
    Assert.assertEquals(2, developers.size());
    Assert.assertEquals(developer1, developers.get(0));
    Assert.assertEquals(developer3, developers.get(1));
  }

  @Test
  public void shouldFindWorkersByRoleId() {
    Role newRole = new Role();
    newRole.setName("SUPERVISOR");

    Developer developer1 = new Developer();
    developer1.setSerialNumber(serialNumber);
    developer1.setName(name);
    developer1.setSurname(surname);
    developer1.setRoles(roles);
    developerRepository.saveAndFlush(developer1);

    Developer developer2 = new Developer();
    developer2.setSerialNumber("111111");
    developer2.setName(name);
    developer2.setSurname(surname);
    developer2.setRoles(new HashSet<Role>(Arrays.asList(newRole)));
    developerRepository.saveAndFlush(developer2);

    Developer developer3 = new Developer();
    developer3.setSerialNumber("222222");
    developer3.setName(name);
    developer3.setSurname(surname);
    developer3.setRoles(roles);
    developerRepository.saveAndFlush(developer3);

    List<Developer> developers = developerRepository.findByRoles_Id(role.getId());
    Assert.assertEquals(2, developers.size());
    Assert.assertEquals(developer1, developers.get(0));
    Assert.assertEquals(developer3, developers.get(1));
  }
}
