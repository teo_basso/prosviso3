package com.example.teamorganizer.repository;

import com.example.teamorganizer.model.Audit;
import com.example.teamorganizer.model.Office;
import com.example.teamorganizer.repository.OfficeRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

import javax.validation.ConstraintViolationException;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class OfficeRepositoryTests {
  String description = "Office of Milan";
  String address = "Piazza Duomo 20";
  String city = "Milan";
  String postalCode = "20121";
  String countryCode = "IT";

  @Autowired
  private OfficeRepository officeRepository;

  @Before
  public void beforeEach() {
    officeRepository.deleteAll();
    officeRepository.flush();
  }

  @Test
  public void shouldBeAnAudit() {
    Assert.assertTrue(Audit.class.isAssignableFrom(Office.class));
  }

  @Test
  public void shouldCreateNewOffices() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity(city);
    office.setPostalCode(postalCode);
    office.setCountryCode(countryCode);
    Assert.assertNull(office.getId());
    officeRepository.saveAndFlush(office);

    Assert.assertEquals(address, office.getAddress());
    Assert.assertEquals(city, office.getCity());
    Assert.assertEquals(postalCode, office.getPostalCode());
    Assert.assertEquals(countryCode, office.getCountryCode());
    Assert.assertNotNull(office.getId());
    Assert.assertNotNull(officeRepository.getOne(office.getId()));
  }

  @Test(expected = JpaObjectRetrievalFailureException.class)
  public void shouldDeleteAOffice() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity(city);
    office.setPostalCode(postalCode);
    office.setCountryCode(countryCode);
    officeRepository.saveAndFlush(office);
    officeRepository.delete(office);
    officeRepository.getOne(office.getId());
  }

  @Test
  public void shouldUpdateAOffice() {
    String newAddress = "Piazza Duomo 21";
    String newCity = "Milano";
    String newPostalCode = "20020";
    String newCountryCode = "IR";
    String newDescription = "new description";

    Office office = new Office();
    office.setAddress(address);
    office.setCity(city);
    office.setPostalCode(postalCode);
    office.setCountryCode(countryCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
    office.setAddress(newAddress);
    office.setCity(newCity);
    office.setPostalCode(newPostalCode);
    office.setCountryCode(newCountryCode);
    office.setDescription(newDescription);
    officeRepository.saveAndFlush(office);

    Office updatedOffice = officeRepository.getOne(office.getId());
    Assert.assertEquals(newAddress, updatedOffice.getAddress());
    Assert.assertEquals(newCity, updatedOffice.getCity());
    Assert.assertEquals(newPostalCode, updatedOffice.getPostalCode());
    Assert.assertEquals(newCountryCode, updatedOffice.getCountryCode());
    Assert.assertEquals(newDescription, updatedOffice.getDescription());
    Assert.assertEquals(office.getId(), updatedOffice.getId());
  }

  @Test
  public void shouldAcceptAnOptionalDescription() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity(city);
    office.setPostalCode(postalCode);
    office.setCountryCode(countryCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);

    Assert.assertEquals(address, office.getAddress());
    Assert.assertEquals(city, office.getCity());
    Assert.assertEquals(postalCode, office.getPostalCode());
    Assert.assertEquals(countryCode, office.getCountryCode());
    Assert.assertEquals(description, office.getDescription());
    Assert.assertEquals(office.getId(), office.getId());
  }

  @Test
  public void shouldGenerateAutoincrementIds() {
    Office office1 = new Office();
    Office office2 = new Office();

    office1.setAddress(address);
    office1.setCity(city);
    office1.setPostalCode(postalCode);
    office1.setCountryCode(countryCode);
    officeRepository.saveAndFlush(office1);

    office2.setAddress(address);
    office2.setCity("Milano");
    office2.setPostalCode(postalCode);
    office2.setCountryCode(countryCode);
    officeRepository.saveAndFlush(office2);

    Assert.assertEquals(office2.getId(), Long.valueOf(office1.getId().longValue() + 1L));
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldThrowIfTriesToInsertToEqualOffices() {
    Office office1 = new Office();
    Office office2 = new Office();

    office1.setAddress(address);
    office1.setCity(city);
    office1.setPostalCode(postalCode);
    office1.setCountryCode(countryCode);
    officeRepository.saveAndFlush(office1);

    office2.setAddress(address);
    office2.setCity(city);
    office2.setPostalCode(postalCode);
    office2.setCountryCode(countryCode);
    officeRepository.saveAndFlush(office2);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfAddressIsNull() {
    Office office = new Office();
    office.setCity(city);
    office.setPostalCode(postalCode);
    office.setCountryCode(countryCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfAddressIsBlank() {
    Office office = new Office();
    office.setAddress("");
    office.setCity(city);
    office.setPostalCode(postalCode);
    office.setCountryCode(countryCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfCityIsNull() {
    Office office = new Office();
    office.setAddress(address);
    office.setPostalCode(postalCode);
    office.setCountryCode(countryCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfCityIsBlank() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity("");
    office.setPostalCode(postalCode);
    office.setCountryCode(countryCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfCityIsShorterThanTwoCharacters() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity("a");
    office.setPostalCode(postalCode);
    office.setCountryCode(countryCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfCityIsLongerThanFiftyCharacters() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity("123456789012345678901234567890123456789012345678901234567890");
    office.setPostalCode(postalCode);
    office.setCountryCode(countryCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfPostalCodeIsNull() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity(city);
    office.setCountryCode(countryCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfPostalCodeIsBlank() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity(city);
    office.setPostalCode("");
    office.setCountryCode(countryCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfPostalCodeIsShorterThanTwoCharacters() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity(city);
    office.setPostalCode("a");
    office.setCountryCode(countryCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfPostalCodeIsLongerThanTenCharacters() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity(city);
    office.setPostalCode("12345678901");
    office.setCountryCode(countryCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfCountryCodeIsNull() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity(city);
    office.setPostalCode(postalCode);
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfCountryCodeIsBlank() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity(city);
    office.setPostalCode(postalCode);
    office.setCountryCode("");
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfCountryCodeIsShorterThanTwoCharacters() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity(city);
    office.setPostalCode(postalCode);
    office.setCountryCode("a");
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfCountryCodeIsLongerThanTwoCharacters() {
    Office office = new Office();
    office.setAddress(address);
    office.setCity(city);
    office.setPostalCode(postalCode);
    office.setCountryCode("123");
    office.setDescription(description);
    officeRepository.saveAndFlush(office);
  }

  @Test
  public void shouldFindOfficesByCity() {
    Office office1 = new Office();
    office1.setAddress(address);
    office1.setCity(city);
    office1.setPostalCode(postalCode);
    office1.setCountryCode(countryCode);
    office1.setDescription(description);
    officeRepository.saveAndFlush(office1);

    Office office2 = new Office();
    office2.setAddress(address);
    office2.setCity("Milano");
    office2.setPostalCode(postalCode);
    office2.setCountryCode(countryCode);
    office2.setDescription(description);
    officeRepository.saveAndFlush(office2);

    List<Office> offices = officeRepository.findByCity(city);
    Assert.assertEquals(1, offices.size());
    Assert.assertEquals(office1, offices.get(0));
  }

  @Test
  public void shouldFindOfficesByPostalCode() {
    Office office1 = new Office();
    office1.setAddress(address);
    office1.setCity(city);
    office1.setPostalCode(postalCode);
    office1.setCountryCode(countryCode);
    office1.setDescription(description);
    officeRepository.saveAndFlush(office1);

    Office office2 = new Office();
    office2.setAddress(address);
    office2.setCity(city);
    office2.setPostalCode("098765");
    office2.setCountryCode(countryCode);
    office2.setDescription(description);
    officeRepository.saveAndFlush(office2);

    List<Office> offices = officeRepository.findByPostalCode("098765");
    Assert.assertEquals(1, offices.size());
    Assert.assertEquals(office2, offices.get(0));
  }

  @Test
  public void shouldFindOfficesByCountryCode() {
    Office office1 = new Office();
    office1.setAddress(address);
    office1.setCity(city);
    office1.setPostalCode(postalCode);
    office1.setCountryCode(countryCode);
    office1.setDescription(description);
    officeRepository.saveAndFlush(office1);

    Office office2 = new Office();
    office2.setAddress(address);
    office2.setCity(city);
    office2.setPostalCode(postalCode);
    office2.setCountryCode("DE");
    office2.setDescription(description);
    officeRepository.saveAndFlush(office2);

    List<Office> offices = officeRepository.findByCountryCode("DE");
    Assert.assertEquals(1, offices.size());
    Assert.assertEquals(office2, offices.get(0));
  }

  @Test
  public void shouldFindOfficesByAddress() {
    String newAddress = "Via Solari 9";

    Office office1 = new Office();
    office1.setAddress(address);
    office1.setCity(city);
    office1.setPostalCode(postalCode);
    office1.setCountryCode(countryCode);
    office1.setDescription(description);
    officeRepository.saveAndFlush(office1);

    Office office2 = new Office();
    office2.setAddress(newAddress);
    office2.setCity(city);
    office2.setPostalCode(postalCode);
    office2.setCountryCode(countryCode);
    office2.setDescription(description);
    officeRepository.saveAndFlush(office2);

    List<Office> offices = officeRepository.findByAddress(newAddress);
    Assert.assertEquals(1, offices.size());
    Assert.assertEquals(office2, offices.get(0));
  }

  @Test
  public void shouldFindOfficesByAddressContaining() {
    Office office1 = new Office();
    office1.setAddress(address);
    office1.setCity(city);
    office1.setPostalCode(postalCode);
    office1.setCountryCode(countryCode);
    office1.setDescription(description);
    officeRepository.saveAndFlush(office1);

    Office office2 = new Office();
    office2.setAddress("Via Solari 9");
    office2.setCity(city);
    office2.setPostalCode(postalCode);
    office2.setCountryCode(countryCode);
    office2.setDescription(description);
    officeRepository.saveAndFlush(office2);

    List<Office> offices = officeRepository.findByAddressContaining("Solari");
    Assert.assertEquals(1, offices.size());
    Assert.assertEquals(office2, offices.get(0));
  }

  @Test
  public void shouldFindOfficesByDescriptionContaining() {
    Office office1 = new Office();
    office1.setAddress(address);
    office1.setCity(city);
    office1.setPostalCode(postalCode);
    office1.setCountryCode(countryCode);
    office1.setDescription("stabilimento principale");
    officeRepository.saveAndFlush(office1);

    Office office2 = new Office();
    office2.setAddress("new address");
    office2.setCity(city);
    office2.setPostalCode(postalCode);
    office2.setCountryCode(countryCode);
    office2.setDescription(description);
    officeRepository.saveAndFlush(office2);

    List<Office> offices = officeRepository.findByDescriptionContaining("principale");
    Assert.assertEquals(1, offices.size());
    Assert.assertEquals(office1, offices.get(0));
  }
}
