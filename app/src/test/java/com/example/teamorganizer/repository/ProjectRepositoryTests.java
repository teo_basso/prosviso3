package com.example.teamorganizer.repository;

import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import com.example.teamorganizer.model.Audit;
import com.example.teamorganizer.model.Project;
import com.example.teamorganizer.model.Office;
import com.example.teamorganizer.repository.ProjectRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class ProjectRepositoryTests {
  String name = "Project 1";
  String version = "1.0.0";
  Office location = new Office();

  @Autowired
  private ProjectRepository projectRepository;

  @Autowired
  private OfficeRepository officeRepository;

  @Before
  public void beforeEach() {
    projectRepository.deleteAll();
    officeRepository.deleteAll();
    projectRepository.flush();
    officeRepository.flush();

    location.setAddress("Piazza Duomo 20");
    location.setCity("Milan");
    location.setPostalCode("20121");
    location.setCountryCode("IT");
    location.setDescription("a description");
  }

  @Test
  public void shouldBeAnAudit() {
    Assert.assertTrue(Audit.class.isAssignableFrom(Project.class));
  }

  @Test
  public void shouldCreateNewProjects() {
    Project project = new Project();
    project.setName(name);
    project.setLocation(location);
    Assert.assertNull(project.getId());
    projectRepository.saveAndFlush(project);

    Assert.assertEquals(name, project.getName());
    Assert.assertEquals(location, project.getLocation());
    Assert.assertNotNull(project.getId());
    Assert.assertNotNull(projectRepository.getOne(project.getId()));
  }

  @Test(expected = JpaObjectRetrievalFailureException.class)
  public void shouldDeleteAProject() {
    Project project = new Project();
    project.setName(name);
    project.setLocation(location);
    projectRepository.saveAndFlush(project);
    projectRepository.delete(project);
    projectRepository.getOne(project.getId());
  }

  @Test
  public void shouldUpdateAProject() {
    String newAddress = "Piazza Duomo 21";
    String newCity = "Milano";
    String newPostalCode = "20020";
    String newCountryCode = "IT";
    String newDescription = "new description";

    String newName = "Project 2";
    Office newLocation = new Office();

    newLocation.setAddress(newAddress);
    newLocation.setCity(newCity);
    newLocation.setPostalCode(newPostalCode);
    newLocation.setCountryCode(newCountryCode);
    newLocation.setDescription(newDescription);

    Project project = new Project();
    project.setName(name);
    project.setLocation(location);
    projectRepository.saveAndFlush(project);
    project.setName(newName);
    project.setLocation(newLocation);
    projectRepository.saveAndFlush(project);

    Project updatedProject = projectRepository.getOne(project.getId());
    Assert.assertEquals(newName, updatedProject.getName());

    Office updatedLocation = updatedProject.getLocation();
    Assert.assertEquals(newAddress, updatedLocation.getAddress());
    Assert.assertEquals(newCity, updatedLocation.getCity());
    Assert.assertEquals(newPostalCode, updatedLocation.getPostalCode());
    Assert.assertEquals(newCountryCode, updatedLocation.getCountryCode());
    Assert.assertEquals(newDescription, updatedLocation.getDescription());

    Assert.assertEquals(project.getId(), updatedProject.getId());
  }

  @Test
  public void shouldAcceptAnOptionalLocation() {
    Project project = new Project();
    project.setName(name);
    project.setLocation(location);
    project.setVersion(version);
    projectRepository.saveAndFlush(project);

    Assert.assertEquals(name, project.getName());
    Assert.assertEquals(location, project.getLocation());
    Assert.assertEquals(version, project.getVersion());
    Assert.assertEquals(project.getId(), project.getId());
  }

  @Test
  public void shouldGenerateAutoincrementIds() {
    Project project1 = new Project();
    Project project2 = new Project();

    project1.setName(name);
    project1.setLocation(location);
    projectRepository.saveAndFlush(project1);

    project2.setName("new name");
    project2.setLocation(location);
    projectRepository.saveAndFlush(project2);

    Assert.assertEquals(project2.getId(), Long.valueOf(project1.getId().longValue() + 1L));
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldThrowIfTriesToInsertEqualNameAndVersion() {
    Project project1 = new Project();
    Project project2 = new Project();

    project1.setName(name);
    project1.setVersion(version);
    projectRepository.saveAndFlush(project1);

    project2.setName(name);
    project1.setVersion(version);
    projectRepository.saveAndFlush(project2);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfNameIsNull() {
    Project project = new Project();
    project.setLocation(location);
    projectRepository.saveAndFlush(project);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfNameIsBlank() {
    Project project = new Project();
    project.setName("");
    project.setLocation(location);
    projectRepository.saveAndFlush(project);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfNameIsShorterThanTwoCharacters() {
    Project project = new Project();
    project.setName("a");
    project.setLocation(location);
    projectRepository.saveAndFlush(project);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfNameIsLongerThanFiftyCharacters() {
    Project project = new Project();
    project.setName("123456789012345678901234567890123456789012345678901234567890");
    project.setLocation(location);
    projectRepository.saveAndFlush(project);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfVersionIsShorterThanOneCharacters() {
    Project project = new Project();
    project.setName(name);
    project.setVersion("");
    projectRepository.saveAndFlush(project);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfVersionIsLongerThanTwentyCharacters() {
    Project project = new Project();
    project.setName(name);
    project.setVersion("123456789012345678901");
    projectRepository.saveAndFlush(project);
  }

  @Test
  public void shouldCreateANewOffice() {
    Assert.assertNull(location.getId());

    Project project = new Project();
    project.setName(name);
    project.setLocation(location);
    projectRepository.saveAndFlush(project);

    Assert.assertNotNull(project.getId());
    Assert.assertNotNull(projectRepository.getOne(project.getId()));
    Assert.assertNotNull(officeRepository.getOne(location.getId()));
  }

  @Test
  public void shouldUseAnExistingOffice() {
    Assert.assertNull(location.getId());

    officeRepository.saveAndFlush(location);

    Long locationId = location.getId();
    Assert.assertNotNull(locationId);

    Project project = new Project();
    project.setName(name);
    project.setLocation(location);
    projectRepository.saveAndFlush(project);

    Assert.assertNotNull(project.getId());
    Assert.assertNotNull(projectRepository.getOne(project.getId()));
    Assert.assertNotNull(officeRepository.getOne(location.getId()));
    Assert.assertEquals(locationId, location.getId());
  }

  @Test
  public void shouldNotDeleteAOffice() {
    Assert.assertNull(location.getId());

    officeRepository.saveAndFlush(location);

    Long locationId = location.getId();
    Assert.assertNotNull(locationId);

    Project project = new Project();
    project.setName(name);
    project.setLocation(location);
    projectRepository.saveAndFlush(project);
    projectRepository.delete(project);

    Assert.assertNotNull(officeRepository.getOne(location.getId()));
    Assert.assertEquals(locationId, location.getId());
  }

  @Test
  public void shouldDeleteAProjectIfDeletesTheOffice() {
    Assert.assertNull(location.getId());

    Project project = new Project();
    project.setName(name);
    project.setLocation(location);
    projectRepository.saveAndFlush(project);
    Assert.assertNotNull(project.getId());

    Office location = project.getLocation();
    Assert.assertNotNull(location.getId());

    officeRepository.deleteAllInBatch();

    Assert.assertFalse(projectRepository.existsById(project.getId()));
  }

  @Test
  public void shouldUpdateAOffice() {
    String newAddress = "Piazza Duomo 21";
    String newCity = "Milano";
    String newPostalCode = "20020";
    String newCountryCode = "IT";
    String newDescription = "new description";

    Office newLocation = new Office();
    newLocation.setAddress("Piazza Duomo 20");
    newLocation.setCity("Milan");
    newLocation.setPostalCode("20000");
    newLocation.setCountryCode("IR");
    newLocation.setDescription("description");

    officeRepository.saveAndFlush(newLocation);

    Long locationId = newLocation.getId();
    Assert.assertNotNull(locationId);

    Project project = new Project();
    project.setName(name);
    project.setLocation(newLocation);
    projectRepository.saveAndFlush(project);

    newLocation.setAddress(newAddress);
    newLocation.setCity(newCity);
    newLocation.setPostalCode(newPostalCode);
    newLocation.setCountryCode(newCountryCode);
    newLocation.setDescription(newDescription);

    Office updatedLocation = projectRepository.saveAndFlush(project).getLocation();

    Assert.assertNotNull(updatedLocation);
    Assert.assertEquals(locationId, updatedLocation.getId());
    Assert.assertEquals(newAddress, updatedLocation.getAddress());
    Assert.assertEquals(newCity, updatedLocation.getCity());
    Assert.assertEquals(newPostalCode, updatedLocation.getPostalCode());
    Assert.assertEquals(newCountryCode, updatedLocation.getCountryCode());
    Assert.assertEquals(newDescription, updatedLocation.getDescription());
  }

  @Test
  public void shouldFindProjectsByName() {
    Project project1 = new Project();
    project1.setName(name);
    project1.setVersion("version");
    project1.setLocation(location);
    projectRepository.saveAndFlush(project1);

    Project project2 = new Project();
    project2.setName("new name");
    project2.setVersion("new version 2");
    project2.setLocation(location);
    projectRepository.saveAndFlush(project2);

    List<Project> projects = projectRepository.findByName("new name");
    Assert.assertEquals(1, projects.size());
    Assert.assertEquals(project2, projects.get(0));
  }

  @Test
  public void shouldFindProjectByNameAndVersion() {
    Project project = new Project();
    project.setName(name);
    project.setVersion(version);
    project.setLocation(location);
    projectRepository.saveAndFlush(project);

    Optional<Project> projectFound = projectRepository.findByNameAndVersion(name, version);
    Assert.assertTrue(projectFound.isPresent());
    Assert.assertEquals(project.getId(), projectFound.get().getId());
  }

  @Test
  public void shouldReturnAnOptionalIfProjectIsNotFoundByNameAndVersion() {
    Optional<Project> projectFound = projectRepository.findByNameAndVersion(name, version);
    Assert.assertFalse(projectFound.isPresent());
  }

  @Test
  public void shouldFindProjectsByVersion() {
    Project project1 = new Project();
    project1.setName(name);
    project1.setVersion("version");
    project1.setLocation(location);
    projectRepository.saveAndFlush(project1);

    Project project2 = new Project();
    project2.setName("new name");
    project2.setVersion("new version 2");
    project2.setLocation(location);
    projectRepository.saveAndFlush(project2);

    List<Project> projects = projectRepository.findByVersion("new version 2");
    Assert.assertEquals(1, projects.size());
    Assert.assertEquals(project2, projects.get(0));
  }

  @Test
  public void shouldFindProjectsByLocation() {
    Office newLocation = new Office();
    newLocation.setAddress("address");
    newLocation.setCity("city");
    newLocation.setCountryCode("IT");
    newLocation.setPostalCode("00000");

    Project project1 = new Project();
    project1.setName(name);
    project1.setLocation(location);
    projectRepository.saveAndFlush(project1);

    Project project2 = new Project();
    project2.setName("new name");
    project2.setLocation(newLocation);
    projectRepository.saveAndFlush(project2);

    Project project3 = new Project();
    project3.setName("new name 2");
    project3.setLocation(location);
    projectRepository.saveAndFlush(project3);

    List<Project> projects = projectRepository.findByLocation(officeRepository.getOne(location.getId()));
    Assert.assertEquals(2, projects.size());
    Assert.assertEquals(project1, projects.get(0));
    Assert.assertEquals(project3, projects.get(1));
    Assert.assertEquals(location.getId(), projects.get(0).getLocation().getId());
  }

  @Test
  public void shouldFindProjectsByVersionContaining() {
    Project project1 = new Project();
    project1.setName(name);
    project1.setLocation(location);
    project1.setVersion("version 1");
    projectRepository.saveAndFlush(project1);

    Project project2 = new Project();
    project2.setName("new name");
    project2.setLocation(location);
    project2.setVersion("version 2");
    projectRepository.saveAndFlush(project2);

    Project project3 = new Project();
    project3.setName("new name 2");
    project3.setLocation(location);
    project3.setVersion("version 12");
    projectRepository.saveAndFlush(project3);

    List<Project> projects = projectRepository.findByVersionContaining("1");
    Assert.assertEquals(2, projects.size());
    Assert.assertEquals(project1, projects.get(0));
    Assert.assertEquals(project3, projects.get(1));
    Assert.assertEquals(location.getId(), projects.get(0).getLocation().getId());
  }
}
