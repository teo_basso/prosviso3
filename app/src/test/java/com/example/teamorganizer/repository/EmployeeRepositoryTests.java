package com.example.teamorganizer.repository;

import com.example.teamorganizer.model.Audit;
import com.example.teamorganizer.model.Employee;
import com.example.teamorganizer.repository.EmployeeRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;

import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class EmployeeRepositoryTests {
  String name = "Matteo";
  String surname = "Basso";
  String serialNumber = "807628";

  @Autowired
  private EmployeeRepository employeeRepository;

  @Before
  public void beforeEach() {
    employeeRepository.deleteAll();
    employeeRepository.flush();
  }

  @Test
  public void shouldBeAnAudit() {
    Assert.assertTrue(Audit.class.isAssignableFrom(Employee.class));
  }

  @Test
  public void shouldCreateNewEmployees() {
    Employee employee = new Employee();
    employee.setSerialNumber(serialNumber);
    employee.setName(name);
    employee.setSurname(surname);
    Assert.assertNull(employee.getId());
    employeeRepository.saveAndFlush(employee);

    Assert.assertEquals(serialNumber, employee.getSerialNumber());
    Assert.assertEquals(name, employee.getName());
    Assert.assertEquals(surname, employee.getSurname());
    Assert.assertNotNull(employee.getId());
    Assert.assertNotNull(employeeRepository.getOne(employee.getId()));
  }

  @Test(expected = JpaObjectRetrievalFailureException.class)
  public void shouldDeleteAEmployee() {
    Employee employee = new Employee();
    employee.setSerialNumber(serialNumber);
    employee.setName(name);
    employee.setSurname(surname);
    employeeRepository.saveAndFlush(employee);
    employeeRepository.delete(employee);
    employeeRepository.getOne(employee.getId());
  }

  @Test
  public void shouldUpdateAEmployee() {
    String newName = "Matteo";
    String newSurname = "Basso";
    String newSerialNumber = "807628";

    Employee employee = new Employee();
    employee.setSerialNumber(serialNumber);
    employee.setName(name);
    employee.setSurname(surname);
    employeeRepository.saveAndFlush(employee);

    employee.setSerialNumber(newSerialNumber);
    employee.setName(newName);
    employee.setSurname(newSurname);
    employeeRepository.saveAndFlush(employee);

    Employee updatedEmployee = employeeRepository.getOne(employee.getId());
    Assert.assertEquals(newSerialNumber, updatedEmployee.getSerialNumber());
    Assert.assertEquals(newName, updatedEmployee.getName());
    Assert.assertEquals(newSurname, updatedEmployee.getSurname());
    Assert.assertEquals(employee.getId(), updatedEmployee.getId());
  }

  @Test
  public void shouldGenerateAutoincrementIds() {
    Employee employee1 = new Employee();
    Employee employee2 = new Employee();

    employee1.setSerialNumber(serialNumber);
    employee1.setName(name);
    employee1.setSurname(name);
    employeeRepository.saveAndFlush(employee1);

    employee2.setSerialNumber("800000");
    employee2.setName(name);
    employee2.setSurname(name);
    employeeRepository.saveAndFlush(employee2);

    Assert.assertEquals(employee2.getId(), Long.valueOf(employee1.getId().longValue() + 1L));
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfSerialNumberIsNull() {
    Employee employee = new Employee();
    employee.setName(name);
    employee.setSurname(surname);
    employeeRepository.saveAndFlush(employee);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfSerialNumberIsBlank() {
    Employee employee = new Employee();
    employee.setSerialNumber("");
    employee.setName(name);
    employee.setSurname(surname);
    employeeRepository.saveAndFlush(employee);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfSerialNumberIsShorterThanSixCharacters() {
    Employee employee = new Employee();
    employee.setSerialNumber("80000");
    employee.setName(name);
    employee.setSurname(surname);
    employeeRepository.saveAndFlush(employee);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfSerialNumberIsLongerThanSixCharacters() {
    Employee employee = new Employee();
    employee.setSerialNumber("123456789012345678901234567890123456789012345678901234567890");
    employee.setName(name);
    employee.setSurname(surname);
    employeeRepository.saveAndFlush(employee);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfNameIsNull() {
    Employee employee = new Employee();
    employee.setSerialNumber(serialNumber);
    employee.setSurname(surname);
    employeeRepository.saveAndFlush(employee);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfNameIsBlank() {
    Employee employee = new Employee();
    employee.setSerialNumber(serialNumber);
    employee.setName("");
    employee.setSurname(surname);
    employeeRepository.saveAndFlush(employee);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfNameIsShorterThanTwoCharacters() {
    Employee employee = new Employee();
    employee.setSerialNumber(serialNumber);
    employee.setName("a");
    employee.setSurname(surname);
    employeeRepository.saveAndFlush(employee);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfNameIsLongerThanFiftyCharacters() {
    Employee employee = new Employee();
    employee.setSerialNumber(serialNumber);
    employee.setName("123456789012345678901234567890123456789012345678901234567890");
    employee.setSurname(surname);
    employeeRepository.saveAndFlush(employee);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfSurnameIsNull() {
    Employee employee = new Employee();
    employee.setSerialNumber(serialNumber);
    employee.setName(surname);
    employeeRepository.saveAndFlush(employee);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfSurnameIsBlank() {
    Employee employee = new Employee();
    employee.setSerialNumber(serialNumber);
    employee.setName(name);
    employee.setSurname("");
    employeeRepository.saveAndFlush(employee);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfSurnameIsShorterThanTwoCharacters() {
    Employee employee = new Employee();
    employee.setSerialNumber(serialNumber);
    employee.setName(name);
    employee.setSurname("a");
    employeeRepository.saveAndFlush(employee);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfSurnameIsLongerThanFiftyCharacters() {
    Employee employee = new Employee();
    employee.setSerialNumber(serialNumber);
    employee.setName(name);
    employee.setSurname("123456789012345678901234567890123456789012345678901234567890");
    employeeRepository.saveAndFlush(employee);
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldThrowIfTriesToInsertEqualSerialNumbers() {
    Employee employee1 = new Employee();
    Employee employee2 = new Employee();

    employee1.setSerialNumber(serialNumber);
    employee1.setName(name);
    employee1.setSurname(surname);
    employeeRepository.saveAndFlush(employee1);

    employee2.setSerialNumber(serialNumber);
    employee2.setName(name);
    employee2.setSurname(surname);
    employeeRepository.saveAndFlush(employee2);
  }

  @Test
  public void shouldFindEmployeesBySerialNumber() {
    Employee employee = new Employee();
    employee.setSerialNumber(serialNumber);
    employee.setName(name);
    employee.setSurname(surname);
    employeeRepository.saveAndFlush(employee);

    Optional<Employee> employeeFound = employeeRepository.findBySerialNumber(serialNumber);
    Assert.assertTrue(employeeFound.isPresent());
    Assert.assertEquals(employee.getId(), employeeFound.get().getId());
  }

  @Test
  public void shouldReturnAnOptionalIfEmployeeIsNotFoundBySerialNumber() {
    Optional<Employee> employeeFound = employeeRepository.findBySerialNumber(serialNumber);
    Assert.assertFalse(employeeFound.isPresent());
  }

  @Test
  public void shouldFindEmployeesByName() {
    Employee employee1 = new Employee();
    employee1.setSerialNumber("111111");
    employee1.setName(name);
    employee1.setSurname(surname);
    employeeRepository.saveAndFlush(employee1);

    Employee employee2 = new Employee();
    employee2.setSerialNumber("222222");
    employee2.setName(name);
    employee2.setSurname(surname);
    employeeRepository.saveAndFlush(employee2);

    Employee employee3 = new Employee();
    employee3.setSerialNumber("333333");
    employee3.setName("name");
    employee3.setSurname(surname);
    employeeRepository.saveAndFlush(employee3);

    List<Employee> employees = employeeRepository.findByName(name);
    Assert.assertEquals(2, employees.size());
    Assert.assertEquals(employee1, employees.get(0));
    Assert.assertEquals(employee2, employees.get(1));
  }

  @Test
  public void shouldFindEmployeesBySurname() {
    Employee employee1 = new Employee();
    employee1.setSerialNumber("111111");
    employee1.setName(name);
    employee1.setSurname(surname);
    employeeRepository.saveAndFlush(employee1);

    Employee employee2 = new Employee();
    employee2.setSerialNumber("222222");
    employee2.setName(name);
    employee2.setSurname("surname");
    employeeRepository.saveAndFlush(employee2);

    Employee employee3 = new Employee();
    employee3.setSerialNumber("333333");
    employee3.setName(name);
    employee3.setSurname(surname);
    employeeRepository.saveAndFlush(employee3);

    List<Employee> employees = employeeRepository.findBySurname(surname);
    Assert.assertEquals(2, employees.size());
    Assert.assertEquals(employee1, employees.get(0));
    Assert.assertEquals(employee3, employees.get(1));
  }

  @Test
  public void shouldFindEmployeesByNameAndSurname() {
    Employee employee1 = new Employee();
    employee1.setSerialNumber("111111");
    employee1.setName(name);
    employee1.setSurname(surname);
    employeeRepository.saveAndFlush(employee1);

    Employee employee2 = new Employee();
    employee2.setSerialNumber("222222");
    employee2.setName(name);
    employee2.setSurname("surname");
    employeeRepository.saveAndFlush(employee2);

    Employee employee3 = new Employee();
    employee3.setSerialNumber("333333");
    employee3.setName("name");
    employee3.setSurname(surname);
    employeeRepository.saveAndFlush(employee3);

    List<Employee> employees = employeeRepository.findByNameAndSurname(name, surname);
    Assert.assertEquals(1, employees.size());
    Assert.assertEquals(employee1, employees.get(0));
  }
}
