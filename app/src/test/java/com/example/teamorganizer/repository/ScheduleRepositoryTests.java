package com.example.teamorganizer.repository;

import com.example.teamorganizer.model.Audit;
import com.example.teamorganizer.model.Employee;
import com.example.teamorganizer.model.Project;
import com.example.teamorganizer.model.Office;
import com.example.teamorganizer.model.Schedule;
import com.example.teamorganizer.repository.ScheduleRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class ScheduleRepositoryTests {
  Long startTS = 1544437436000L;
  Long endTS = 1544464800000L;
  Date start = new Date(startTS);
  Date end = new Date(endTS);
  Employee employee = new Employee();
  Project project = new Project();
  Office location = new Office();

  String serialNumber = "807628";
  String name = "Matteo";
  String surname = "Basso";

  String projectName = "Project 1";
  String model = "1.0.0";

  String address = "Piazza Duomo 20";
  String city = "Milan";
  String countryCode = "IT";
  String postalCode = "20121";

  @Autowired
  private ScheduleRepository scheduleRepository;

  @Autowired
  private EmployeeRepository employeeRepository;

  @Autowired
  private ProjectRepository projectRepository;

  @Before
  public void beforeEach() {
    scheduleRepository.deleteAll();
    employeeRepository.deleteAll();
    projectRepository.deleteAll();
    scheduleRepository.flush();
    employeeRepository.flush();
    projectRepository.flush();

    employee.setSerialNumber(serialNumber);
    employee.setName(name);
    employee.setSurname(surname);

    location.setAddress(address);
    location.setCity(city);
    location.setCountryCode(countryCode);
    location.setPostalCode(postalCode);

    project.setName(projectName);
    project.setVersion(model);
    project.setLocation(location);
  }

  @Test
  public void shouldBeAnAudit() {
    Assert.assertTrue(Audit.class.isAssignableFrom(Schedule.class));
  }

  @Test
  public void shouldCreateNewSchedules() {
    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    Assert.assertNull(schedule.getId());
    scheduleRepository.saveAndFlush(schedule);

    Assert.assertEquals(start, schedule.getStart());
    Assert.assertEquals(end, schedule.getEnd());
    Assert.assertEquals(employee, schedule.getEmployee());
    Assert.assertNotNull(schedule.getId());
    Assert.assertNotNull(scheduleRepository.getOne(schedule.getId()));
  }

  @Test(expected = JpaObjectRetrievalFailureException.class)
  public void shouldDeleteASchedule() {
    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    Assert.assertNull(schedule.getId());
    scheduleRepository.saveAndFlush(schedule);
    scheduleRepository.delete(schedule);
    scheduleRepository.getOne(schedule.getId());
  }

  @Test
  public void shouldUpdateASchedule() {
    Date newStart = new Date(1544437436030L);
    Date newEnd = new Date(1544464800090L);
    String newSerialNumber = "888888";
    Employee newEmployee = new Employee();

    newEmployee.setSerialNumber(newSerialNumber);
    newEmployee.setName(name);
    newEmployee.setSurname(surname);

    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    scheduleRepository.saveAndFlush(schedule);
    schedule.setStart(newStart);
    schedule.setEnd(newEnd);
    schedule.setEmployee(newEmployee);
    scheduleRepository.saveAndFlush(schedule);

    Schedule updatedSchedule = scheduleRepository.getOne(schedule.getId());
    Assert.assertEquals(newStart, updatedSchedule.getStart());
    Assert.assertEquals(newEnd, updatedSchedule.getEnd());
    Assert.assertEquals(newEmployee, updatedSchedule.getEmployee());
    Assert.assertEquals(schedule.getId(), updatedSchedule.getId());
  }

  @Test
  public void shouldGenerateAutoincrementIds() {
    Schedule schedule1 = new Schedule();
    Schedule schedule2 = new Schedule();

    schedule1.setStart(start);
    schedule1.setEnd(end);
    schedule1.setEmployee(employee);
    scheduleRepository.saveAndFlush(schedule1);

    schedule2.setStart(start);
    schedule2.setEnd(end);
    schedule2.setEmployee(employee);
    scheduleRepository.saveAndFlush(schedule2);

    Assert.assertEquals(schedule2.getId(), Long.valueOf(schedule1.getId().longValue() + 1L));
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldThrowIfStartIsGreaterThanEnd() {
    Schedule schedule = new Schedule();
    schedule.setStart(end);
    schedule.setEnd(start);
    schedule.setEmployee(employee);
    Assert.assertNull(schedule.getId());
    scheduleRepository.saveAndFlush(schedule);
  }

  @Test
  public void shouldAcceptAnOptionalProject() {
    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    schedule.setProject(project);
    Assert.assertNull(schedule.getId());
    scheduleRepository.saveAndFlush(schedule);

    Assert.assertEquals(start, schedule.getStart());
    Assert.assertEquals(end, schedule.getEnd());
    Assert.assertEquals(employee, schedule.getEmployee());
    Assert.assertEquals(project, schedule.getProject());
    Assert.assertNotNull(schedule.getId());
    Assert.assertNotNull(scheduleRepository.getOne(schedule.getId()));
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldThrowIfEmployeeIsNull() {
    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    scheduleRepository.saveAndFlush(schedule);
  }

  @Test
  public void shouldCreateANewEmployee() {
    Assert.assertNull(employee.getId());

    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    scheduleRepository.saveAndFlush(schedule);

    Assert.assertNotNull(schedule.getId());
    Assert.assertNotNull(scheduleRepository.getOne(schedule.getId()));
    Assert.assertNotNull(employeeRepository.getOne(employee.getId()));
  }

  @Test
  public void shouldUseAnExistingEmployee() {
    Assert.assertNull(employee.getId());

    employeeRepository.saveAndFlush(employee);

    Long employeeId = employee.getId();
    Assert.assertNotNull(employeeId);

    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    scheduleRepository.saveAndFlush(schedule);

    Assert.assertNotNull(schedule.getId());
    Assert.assertNotNull(scheduleRepository.getOne(schedule.getId()));
    Assert.assertNotNull(employeeRepository.getOne(employee.getId()));
    Assert.assertEquals(employeeId, employee.getId());
  }

  @Test
  public void shouldNotDeleteAnEmployee() {
    Assert.assertNull(employee.getId());

    employeeRepository.saveAndFlush(employee);

    Long employeeId = employee.getId();
    Assert.assertNotNull(employeeId);

    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    scheduleRepository.saveAndFlush(schedule);
    scheduleRepository.delete(schedule);

    Assert.assertNotNull(employeeRepository.getOne(employee.getId()));
    Assert.assertEquals(employeeId, employee.getId());
  }

  @Test
  public void shouldUpdateAnEmployee() {
    String newSerialNumber = "123456";
    String newName = "Mario";
    String newSurname = "Rossi";

    Employee newEmployee = new Employee();
    newEmployee.setSerialNumber(serialNumber);
    newEmployee.setName(name);
    newEmployee.setSurname(surname);

    employeeRepository.saveAndFlush(newEmployee);

    Long employeeId = newEmployee.getId();
    Assert.assertNotNull(employeeId);

    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(newEmployee);
    scheduleRepository.saveAndFlush(schedule);

    newEmployee.setSerialNumber(newSerialNumber);
    newEmployee.setName(newName);
    newEmployee.setSurname(newSurname);

    Employee updatedEmployee = scheduleRepository.saveAndFlush(schedule).getEmployee();

    Assert.assertNotNull(updatedEmployee);
    Assert.assertEquals(employeeId, updatedEmployee.getId());
    Assert.assertEquals(newSerialNumber, updatedEmployee.getSerialNumber());
    Assert.assertEquals(newName, updatedEmployee.getName());
    Assert.assertEquals(newSurname, updatedEmployee.getSurname());
  }

  @Test
  public void shouldCreateANewProject() {
    Assert.assertNull(project.getId());

    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    schedule.setProject(project);
    scheduleRepository.saveAndFlush(schedule);

    Assert.assertNotNull(schedule.getId());
    Assert.assertNotNull(scheduleRepository.getOne(schedule.getId()));
    Assert.assertNotNull(projectRepository.getOne(project.getId()));
  }

  @Test
  public void shouldUseAnExistingProject() {
    Assert.assertNull(project.getId());

    projectRepository.saveAndFlush(project);

    Long projectId = project.getId();
    Assert.assertNotNull(projectId);

    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    schedule.setProject(project);
    scheduleRepository.saveAndFlush(schedule);

    Assert.assertNotNull(schedule.getId());
    Assert.assertNotNull(scheduleRepository.getOne(schedule.getId()));
    Assert.assertNotNull(projectRepository.getOne(project.getId()));
    Assert.assertEquals(projectId, project.getId());
  }

  @Test
  public void shouldNotDeleteAProject() {
    Assert.assertNull(project.getId());

    projectRepository.saveAndFlush(project);

    Long projectId = project.getId();
    Assert.assertNotNull(projectId);

    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    schedule.setProject(project);
    scheduleRepository.saveAndFlush(schedule);
    scheduleRepository.delete(schedule);

    Assert.assertNotNull(projectRepository.getOne(project.getId()));
    Assert.assertEquals(projectId, project.getId());
  }

  @Test
  public void shouldUpdateAProject() {
    String newName = "new name";
    String newVersion = "1.1.0";
    Office newLocation = new Office();

    newLocation.setAddress("Piazza Duomo 23");
    newLocation.setCity("Milano");
    newLocation.setCountryCode("IT");
    newLocation.setPostalCode("20000");

    Project newProject = new Project();
    newProject.setName(newName);
    newProject.setVersion(name);
    newProject.setLocation(location);

    projectRepository.saveAndFlush(newProject);

    Long projectId = newProject.getId();
    Assert.assertNotNull(projectId);

    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    schedule.setProject(newProject);
    scheduleRepository.saveAndFlush(schedule);

    newProject.setName(newName);
    newProject.setVersion(newVersion);
    newProject.setLocation(newLocation);

    Project updatedProject = scheduleRepository.saveAndFlush(schedule).getProject();

    Assert.assertNotNull(updatedProject);
    Assert.assertEquals(projectId, updatedProject.getId());
    Assert.assertEquals(newName, updatedProject.getName());
    Assert.assertEquals(newVersion, updatedProject.getVersion());
    Assert.assertEquals(newLocation, updatedProject.getLocation());
  }

  @Test
  public void shouldDeleteAScheduleIfDeletesTheEmployee() {
    Assert.assertNull(employee.getId());

    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    schedule.setProject(project);
    scheduleRepository.saveAndFlush(schedule);
    Assert.assertNotNull(schedule.getId());

    Employee insertedEmployee = schedule.getEmployee();
    Assert.assertNotNull(insertedEmployee.getId());

    employeeRepository.deleteAllInBatch();

    Assert.assertFalse(scheduleRepository.existsById(schedule.getId()));
  }

  @Test
  public void shouldDeleteAScheduleIfDeletesTheProject() {
    Assert.assertNull(project.getId());

    Schedule schedule = new Schedule();
    schedule.setStart(start);
    schedule.setEnd(end);
    schedule.setEmployee(employee);
    schedule.setProject(project);
    scheduleRepository.saveAndFlush(schedule);
    Assert.assertNotNull(schedule.getId());

    Project insertedProject = schedule.getProject();
    Assert.assertNotNull(insertedProject.getId());

    projectRepository.deleteAllInBatch();

    Assert.assertFalse(scheduleRepository.existsById(schedule.getId()));
  }

  @Test
  public void shouldFindProjectsByEmployee() {
    Employee newEmployee = new Employee();
    newEmployee.setSerialNumber("097463");
    newEmployee.setName(name);
    newEmployee.setSurname(surname);

    Schedule schedule1 = new Schedule();
    schedule1.setStart(start);
    schedule1.setEnd(end);
    schedule1.setEmployee(newEmployee);
    scheduleRepository.saveAndFlush(schedule1);

    Schedule schedule2 = new Schedule();
    schedule2.setStart(start);
    schedule2.setEnd(end);
    schedule2.setEmployee(employee);
    scheduleRepository.saveAndFlush(schedule2);

    List<Schedule> schedules = scheduleRepository.findByEmployee(employee);
    Assert.assertEquals(1, schedules.size());
    Assert.assertEquals(schedule2, schedules.get(0));
  }

  @Test
  public void shouldFindProjectsByProject() {
    Project newProject = new Project();
    newProject.setName("project name");
    newProject.setVersion(model);
    newProject.setLocation(location);

    Schedule schedule1 = new Schedule();
    schedule1.setStart(start);
    schedule1.setEnd(end);
    schedule1.setEmployee(employee);
    schedule1.setProject(project);
    scheduleRepository.saveAndFlush(schedule1);

    Schedule schedule2 = new Schedule();
    schedule2.setStart(start);
    schedule2.setEnd(end);
    schedule2.setEmployee(employee);
    schedule2.setProject(newProject);
    scheduleRepository.saveAndFlush(schedule2);

    List<Schedule> schedules = scheduleRepository.findByProject(project);
    Assert.assertEquals(1, schedules.size());
    Assert.assertEquals(schedule1, schedules.get(0));
  }

  @Test
  public void shouldFindProjectsByStartBetween() {
    Project newProject = new Project();
    newProject.setName("project name");
    newProject.setVersion(model);
    newProject.setLocation(location);

    Schedule schedule1 = new Schedule();
    schedule1.setStart(new Date(startTS + 45L));
    schedule1.setEnd(end);
    schedule1.setEmployee(employee);
    schedule1.setProject(project);
    scheduleRepository.saveAndFlush(schedule1);

    Schedule schedule2 = new Schedule();
    schedule2.setStart(start);
    schedule2.setEnd(end);
    schedule2.setEmployee(employee);
    schedule2.setProject(newProject);
    scheduleRepository.saveAndFlush(schedule2);

    List<Schedule> schedules = scheduleRepository.findByStartBetween(new Date(startTS + 30L), new Date(startTS + 60L));
    Assert.assertEquals(1, schedules.size());
    Assert.assertEquals(schedule1, schedules.get(0));
  }

  @Test
  public void shouldFindProjectsByEndBetween() {
    Project newProject = new Project();
    newProject.setName("project name");
    newProject.setVersion(model);
    newProject.setLocation(location);

    Schedule schedule1 = new Schedule();
    schedule1.setStart(start);
    schedule1.setEnd(new Date(endTS + 5L));
    schedule1.setEmployee(employee);
    schedule1.setProject(project);
    scheduleRepository.saveAndFlush(schedule1);

    Schedule schedule2 = new Schedule();
    schedule2.setStart(start);
    schedule2.setEnd(new Date(endTS + 10L));
    schedule2.setEmployee(employee);
    schedule2.setProject(newProject);
    scheduleRepository.saveAndFlush(schedule2);

    List<Schedule> schedules = scheduleRepository.findByEndBetween(new Date(endTS + 5L), new Date(endTS + 9L));
    Assert.assertEquals(1, schedules.size());
    Assert.assertEquals(schedule1, schedules.get(0));
  }
}
