package com.example.teamorganizer.repository;

import java.util.List;
import java.util.Optional;

import com.example.teamorganizer.model.Audit;
import com.example.teamorganizer.model.Employee;
import com.example.teamorganizer.model.OfficeWorker;
import com.example.teamorganizer.repository.OfficeWorkerRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class OfficeWorkerRepositoryTests {
  String name = "Matteo";
  String surname = "Basso";
  String serialNumber = "807628";
  OfficeWorker manager = new OfficeWorker();

  @Autowired
  private OfficeWorkerRepository officeDeveloperRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Before
  public void beforeEach() {
    officeDeveloperRepository.deleteAll();
    roleRepository.deleteAll();
    officeDeveloperRepository.flush();
    roleRepository.flush();

    manager.setSerialNumber("000000");
    manager.setName("Mario");
    manager.setSurname("Rossi");
  }

  @Test
  public void shouldBeAnAudit() {
    Assert.assertTrue(Audit.class.isAssignableFrom(OfficeWorker.class));
  }

  @Test
  public void shouldBeAnEmployee() {
    Assert.assertTrue(Employee.class.isAssignableFrom(OfficeWorker.class));
  }

  @Test
  public void shouldCreateNewOfficeWorkers() {
    OfficeWorker officeWorker = new OfficeWorker();
    officeWorker.setSerialNumber(serialNumber);
    officeWorker.setName(name);
    officeWorker.setSurname(surname);
    Assert.assertNull(officeWorker.getId());
    officeDeveloperRepository.saveAndFlush(officeWorker);

    Assert.assertEquals(serialNumber, officeWorker.getSerialNumber());
    Assert.assertEquals(name, officeWorker.getName());
    Assert.assertEquals(surname, officeWorker.getSurname());
    Assert.assertNotNull(officeWorker.getId());
    Assert.assertNotNull(officeDeveloperRepository.getOne(officeWorker.getId()));
  }

  @Test(expected = JpaObjectRetrievalFailureException.class)
  public void shouldDeleteAOfficeWorker() {
    OfficeWorker officeWorker = new OfficeWorker();
    officeWorker.setSerialNumber(serialNumber);
    officeWorker.setName(name);
    officeWorker.setSurname(surname);
    officeDeveloperRepository.saveAndFlush(officeWorker);
    officeDeveloperRepository.delete(officeWorker);
    officeDeveloperRepository.getOne(officeWorker.getId());
  }

  @Test
  public void shouldUpdateAOfficeWorker() {
    String newName = "Matteo";
    String newSurname = "Basso";
    String newSerialNumber = "807628";
    OfficeWorker newManager = new OfficeWorker();
    newManager.setName(name);
    newManager.setSerialNumber("999999");
    newManager.setName(name);
    newManager.setSurname(surname);

    OfficeWorker officeWorker = new OfficeWorker();
    officeWorker.setSerialNumber(serialNumber);
    officeWorker.setName(name);
    officeWorker.setSurname(surname);
    officeWorker.setManager(manager);
    officeDeveloperRepository.saveAndFlush(officeWorker);

    officeWorker.setSerialNumber(newSerialNumber);
    officeWorker.setName(newName);
    officeWorker.setSurname(newSurname);
    officeWorker.setManager(newManager);
    officeDeveloperRepository.saveAndFlush(officeWorker);

    OfficeWorker updatedOfficeWorker = officeDeveloperRepository.getOne(officeWorker.getId());
    Assert.assertEquals(newSerialNumber, updatedOfficeWorker.getSerialNumber());
    Assert.assertEquals(newName, updatedOfficeWorker.getName());
    Assert.assertEquals(newSurname, updatedOfficeWorker.getSurname());
    Assert.assertEquals(officeWorker.getId(), updatedOfficeWorker.getId());

    OfficeWorker updatedManager = updatedOfficeWorker.getManager();
    Assert.assertEquals(newManager.getId(), updatedManager.getId());
  }

  @Test
  public void shouldAcceptAnOptionalManager() {
    OfficeWorker officeWorker = new OfficeWorker();
    officeWorker.setSerialNumber(serialNumber);
    officeWorker.setName(name);
    officeWorker.setSurname(surname);
    officeWorker.setManager(manager);
    Assert.assertNull(officeWorker.getId());
    officeDeveloperRepository.saveAndFlush(officeWorker);

    Assert.assertEquals(serialNumber, officeWorker.getSerialNumber());
    Assert.assertEquals(name, officeWorker.getName());
    Assert.assertEquals(surname, officeWorker.getSurname());
    Assert.assertEquals(manager, officeWorker.getManager());
    Assert.assertNotNull(officeWorker.getId());
    Assert.assertNotNull(officeDeveloperRepository.getOne(officeWorker.getId()));
  }

  @Test
  public void shouldCreateANewManager() {
    Assert.assertNull(manager.getId());

    OfficeWorker officeWorker = new OfficeWorker();
    officeWorker.setSerialNumber(serialNumber);
    officeWorker.setName(name);
    officeWorker.setSurname(name);
    officeWorker.setManager(manager);
    officeDeveloperRepository.saveAndFlush(officeWorker);

    Assert.assertNotNull(officeWorker.getId());
    Assert.assertNotNull(officeDeveloperRepository.getOne(officeWorker.getId()));
    Assert.assertNotNull(officeDeveloperRepository.getOne(manager.getId()));
  }

  @Test
  public void shouldUseAnExistingManager() {
    Assert.assertNull(manager.getId());

    officeDeveloperRepository.saveAndFlush(manager);

    Long managerId = manager.getId();
    Assert.assertNotNull(managerId);

    OfficeWorker officeWorker = new OfficeWorker();
    officeWorker.setSerialNumber(serialNumber);
    officeWorker.setName(name);
    officeWorker.setSurname(name);
    officeWorker.setManager(manager);
    officeDeveloperRepository.saveAndFlush(officeWorker);

    Assert.assertNotNull(officeWorker.getId());
    Assert.assertNotNull(officeDeveloperRepository.getOne(officeWorker.getId()));
    Assert.assertNotNull(officeDeveloperRepository.getOne(manager.getId()));
    Assert.assertEquals(managerId, manager.getId());
  }

  @Test
  public void shouldNotDeleteAManager() {
    Assert.assertNull(manager.getId());

    officeDeveloperRepository.saveAndFlush(manager);

    Long managerId = manager.getId();
    Assert.assertNotNull(managerId);

    OfficeWorker officeWorker = new OfficeWorker();
    officeWorker.setSerialNumber(serialNumber);
    officeWorker.setName(name);
    officeWorker.setSurname(name);
    officeWorker.setManager(manager);
    officeDeveloperRepository.saveAndFlush(officeWorker);
    officeDeveloperRepository.delete(officeWorker);

    Assert.assertNotNull(officeDeveloperRepository.getOne(manager.getId()));
    Assert.assertEquals(managerId, manager.getId());
  }

  @Test
  public void shouldUpdateAManager() {
    String newName = "Giovanni";

    OfficeWorker newManager = new OfficeWorker();
    newManager.setName("Luca");
    newManager.setSerialNumber("000000");
    newManager.setSurname(surname);

    officeDeveloperRepository.saveAndFlush(newManager);

    Long managerId = newManager.getId();
    Assert.assertNotNull(managerId);

    OfficeWorker officeWorker = new OfficeWorker();
    officeWorker.setSerialNumber(serialNumber);
    officeWorker.setName(name);
    officeWorker.setSurname(name);
    officeWorker.setManager(newManager);
    officeDeveloperRepository.saveAndFlush(officeWorker);

    newManager.setName(newName);

    OfficeWorker updatedManager = officeDeveloperRepository.saveAndFlush(officeWorker).getManager();

    Assert.assertNotNull(updatedManager);
    Assert.assertEquals(managerId, updatedManager.getId());
    Assert.assertEquals(newName, updatedManager.getName());
  }

  @Test
  public void shouldFindOfficeWorkersByManager() {
    OfficeWorker manager = new OfficeWorker();
    manager.setSerialNumber("000000");
    manager.setName(name);
    manager.setSurname(surname);

    OfficeWorker worker1 = new OfficeWorker();
    worker1.setSerialNumber("111111");
    worker1.setName(name);
    worker1.setSurname(surname);
    worker1.setManager(manager);
    officeDeveloperRepository.saveAndFlush(worker1);

    OfficeWorker worker2 = new OfficeWorker();
    worker2.setSerialNumber("222222");
    worker2.setName(name);
    worker2.setSurname(surname);
    worker2.setManager(worker1);
    officeDeveloperRepository.saveAndFlush(worker2);

    OfficeWorker worker3 = new OfficeWorker();
    worker3.setSerialNumber("333333");
    worker3.setName(name);
    worker3.setSurname(surname);
    worker3.setManager(manager);
    officeDeveloperRepository.saveAndFlush(worker3);

    List<OfficeWorker> projects = officeDeveloperRepository.findByManager(officeDeveloperRepository.getOne(manager.getId()));
    Assert.assertEquals(2, projects.size());
    Assert.assertEquals(worker1, projects.get(0));
    Assert.assertEquals(worker3, projects.get(1));
    Assert.assertEquals(manager.getId(), projects.get(0).getManager().getId());
  }

  @Test
  public void shouldFindOfficeWorkersByManagerId() {
    OfficeWorker manager = new OfficeWorker();
    manager.setSerialNumber("000000");
    manager.setName(name);
    manager.setSurname(surname);

    OfficeWorker worker1 = new OfficeWorker();
    worker1.setSerialNumber("111111");
    worker1.setName(name);
    worker1.setSurname(surname);
    worker1.setManager(manager);
    officeDeveloperRepository.saveAndFlush(worker1);

    OfficeWorker worker2 = new OfficeWorker();
    worker2.setSerialNumber("222222");
    worker2.setName(name);
    worker2.setSurname(surname);
    worker2.setManager(worker1);
    officeDeveloperRepository.saveAndFlush(worker2);

    OfficeWorker worker3 = new OfficeWorker();
    worker3.setSerialNumber("333333");
    worker3.setName(name);
    worker3.setSurname(surname);
    worker3.setManager(manager);
    officeDeveloperRepository.saveAndFlush(worker3);

    List<OfficeWorker> projects = officeDeveloperRepository.findByManagerId(manager.getId());
    Assert.assertEquals(2, projects.size());
    Assert.assertEquals(worker1, projects.get(0));
    Assert.assertEquals(worker3, projects.get(1));
    Assert.assertEquals(manager.getId(), projects.get(0).getManager().getId());
  }

  @Test
  public void shouldExtendEmployeeCustomRepository() {
    Assert.assertTrue(officeDeveloperRepository instanceof EmployeeRepositoryCustom<?>);
    Optional<OfficeWorker> officeWorker = officeDeveloperRepository.findBySerialNumber(serialNumber);
  }
}
