package com.example.teamorganizer.repository;

import com.example.teamorganizer.model.Audit;
import com.example.teamorganizer.model.Role;
import com.example.teamorganizer.repository.RoleRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class RoleRepositoryTests {
  String name = "PYTHON_DEVELOPER";

  @Autowired
  private RoleRepository roleRepository;

  @Before
  public void beforeEach() {
    roleRepository.deleteAll();
    roleRepository.flush();
  }

  @Test
  public void shouldBeAnAudit() {
    Assert.assertTrue(Audit.class.isAssignableFrom(Role.class));
  }

  @Test
  public void shouldCreateNewRoles() {
    Role role = new Role();
    role.setName(name);
    Assert.assertNull(role.getId());
    roleRepository.saveAndFlush(role);

    Assert.assertEquals(name, role.getName());
    Assert.assertNotNull(role.getId());
    Assert.assertNotNull(roleRepository.getOne(role.getId()));
  }

  @Test(expected = JpaObjectRetrievalFailureException.class)
  public void shouldDeleteARole() {
    Role role = new Role();
    role.setName(name);
    roleRepository.saveAndFlush(role);
    roleRepository.delete(role);
    roleRepository.getOne(role.getId());
  }

  @Test
  public void shouldUpdateARole() {
    String description = "description";
    String newName = "PYTHON_DEVELOPER";
    String newDescription = "new description";

    Role role = new Role();
    role.setName(name);
    role.setDescription(description);
    roleRepository.saveAndFlush(role);
    role.setName(newName);
    role.setDescription(newDescription);
    roleRepository.saveAndFlush(role);

    Role updatedRole = roleRepository.getOne(role.getId());
    Assert.assertEquals(newName, updatedRole.getName());
    Assert.assertEquals(newDescription, updatedRole.getDescription());
    Assert.assertEquals(role.getId(), updatedRole.getId());
  }

  @Test
  public void shouldAcceptAnOptionalDescription() {
    String description = "python team lead";

    Role role = new Role();
    role.setName(name);
    role.setDescription(description);
    roleRepository.saveAndFlush(role);

    Assert.assertEquals(name, role.getName());
    Assert.assertEquals(description, role.getDescription());
    Assert.assertEquals(role.getId(), role.getId());
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldThrowIfANonUniqueNameIsProvided() {
    Role role = new Role();
    role.setName(name);
    roleRepository.saveAndFlush(role);

    Role role2 = new Role();
    role2.setName(name);
    roleRepository.saveAndFlush(role2);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfNameIsNull() {
    Role role = new Role();
    roleRepository.saveAndFlush(role);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfNameIsBlank() {
    Role role = new Role();
    role.setName("");
    roleRepository.saveAndFlush(role);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfNameIsShorterThanTwoCharacters() {
    Role role = new Role();
    role.setName("a");
    roleRepository.saveAndFlush(role);
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowIfNameIsLongerThanFiftyCharacters() {
    Role role = new Role();
    role.setName("123456789012345678901234567890123456789012345678901234567890");
    roleRepository.saveAndFlush(role);
  }

  @Test
  public void shouldGenerateAutoincrementIds() {
    Role role1 = new Role();
    Role role2 = new Role();

    role1.setName(name);
    roleRepository.saveAndFlush(role1);

    role2.setName("PYTHON_TEAM_LEAD");
    roleRepository.saveAndFlush(role2);

    Assert.assertEquals(role2.getId(), Long.valueOf(role1.getId().longValue() + 1L));
  }

  @Test
  public void shouldFindRoleByName() {
    Role role = new Role();
    role.setName(name);
    roleRepository.saveAndFlush(role);

    Optional<Role> roleFound = roleRepository.findByName(name);
    Assert.assertTrue(roleFound.isPresent());
    Assert.assertEquals(role.getId(), roleFound.get().getId());
  }

  @Test
  public void shouldReturnAnOptionalIfRoleIsNotFoundByName() {
    Optional<Role> roleFound = roleRepository.findByName(name);
    Assert.assertFalse(roleFound.isPresent());
  }

  @Test
  public void shouldFindRolesByDescriptionContaining() {
    Role role1 = new Role();
    role1.setName("C_LEAD");
    role1.setDescription("C team lead");
    roleRepository.saveAndFlush(role1);

    Role role2 = new Role();
    role2.setName("PYTHON_DEVELOPER");
    role2.setDescription("python junior developer");
    roleRepository.saveAndFlush(role2);

    Role role3 = new Role();
    role3.setName("PYTHON_LEAD");
    role3.setDescription("python team lead");
    roleRepository.saveAndFlush(role3);

    List<Role> offices = roleRepository.findByDescriptionContaining("lead");
    Assert.assertEquals(2, offices.size());
    Assert.assertEquals(role1, offices.get(0));
    Assert.assertEquals(role3, offices.get(1));
  }
}
