package com.example.teamorganizer.mock;

import com.example.teamorganizer.model.Audit;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "auditImpl")
public class AuditImpl extends Audit implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String value;

  public Long getId() {
      return id;
  }

  public void setId(Long id) {
      this.id = id;
  }

  public String getValue() {
      return value;
  }

  public void setValue(String value) {
      this.value = value;
  }
}
