package com.example.teamorganizer.mock;

import com.example.teamorganizer.mock.AuditImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditImplRepository extends JpaRepository<AuditImpl, Long> {
}
