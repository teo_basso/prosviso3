package com.example.teamorganizer.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.example.teamorganizer.model.Office;
import com.example.teamorganizer.model.Project;
import com.example.teamorganizer.service.ProjectService;
import com.opensymphony.xwork2.ActionContext;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Component;

@Component
public class ProjectAction extends Action<Project, ProjectService> {

  private static final long serialVersionUID = 1L;

  private List<Office> offices = new ArrayList<Office>();

  public ProjectAction() {
    super(new Project());
  }

  @Override
	public String saveOrUpdate() {
    HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
    Office office = ((ProjectService) service).findOffice(Long.parseLong(request.getParameter("locationId")));

    Project value = getValue();
    value.setLocation(office);
		service.save(value);
		return SUCCESS;
  }

  @Override
	public String list() {
    setList(service.findAll());
    offices = ((ProjectService) service).findAllOffices();
		return SUCCESS;
	}

  @Override
	public String edit() {
		HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
		setValue(service.getOne(Long.parseLong(request.getParameter("id"))));
    setList(service.findAll());
    offices = ((ProjectService) service).findAllOffices();
		return SUCCESS;
	}

	public List<Office> getOffices() {
		return offices;
	}

	public void setOffices(List<Office> offices) {
		this.offices = offices;
	}
}
