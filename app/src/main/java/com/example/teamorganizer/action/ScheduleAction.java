package com.example.teamorganizer.action;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.example.teamorganizer.model.Employee;
import com.example.teamorganizer.model.Project;
import com.example.teamorganizer.model.Schedule;
import com.example.teamorganizer.service.ScheduleService;
import com.opensymphony.xwork2.ActionContext;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Component;

@Component
public class ScheduleAction extends Action<Schedule, ScheduleService> {

  private static final long serialVersionUID = 1L;

  private List<Employee> employees = new ArrayList<Employee>();
  private List<Project> projects = new ArrayList<Project>();

  public ScheduleAction() {
    super(new Schedule());
  }

  @Override
	public String saveOrUpdate() {
    Schedule value = getValue();
    HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
    Employee employee = ((ScheduleService) service).findEmployee(Long.parseLong(request.getParameter("employeeId")));
    value.setEmployee(employee);

    String projectId = request.getParameter("projectId");
    Project project = null;
    if (projectId != null && !projectId.trim().isEmpty()) {
      project = ((ScheduleService) service).findProject(Long.parseLong(projectId));
    }
    value.setProject(project);

    try {
      DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
      value.setStart(dateFormat.parse(request.getParameter("start")));
      value.setEnd(dateFormat.parse(request.getParameter("end")));
    } catch (ParseException ex) {
      throw new Error(ex.getMessage());
    }

		service.save(value);
		return SUCCESS;
  }

  @Override
	public String list() {
    setList(service.findAll());
    setEmployees(((ScheduleService) service).findAllEmployees());
    setProjects(((ScheduleService) service).findAllProjects());
		return SUCCESS;
	}

  @Override
	public String edit() {
		HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
		setValue(service.getOne(Long.parseLong(request.getParameter("id"))));
    setList(service.findAll());
    setEmployees(((ScheduleService) service).findAllEmployees());
    setProjects(((ScheduleService) service).findAllProjects());;
		return SUCCESS;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
}
