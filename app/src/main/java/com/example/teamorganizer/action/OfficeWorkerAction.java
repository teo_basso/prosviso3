package com.example.teamorganizer.action;

import javax.servlet.http.HttpServletRequest;

import com.example.teamorganizer.model.OfficeWorker;
import com.example.teamorganizer.service.OfficeWorkerService;
import com.opensymphony.xwork2.ActionContext;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Component;

@Component
public class OfficeWorkerAction extends Action<OfficeWorker, OfficeWorkerService> {
  private static final long serialVersionUID = 1L;

  public OfficeWorkerAction() {
    super(new OfficeWorker());
  }

  @Override
	public String saveOrUpdate() {
    OfficeWorker value = getValue();
    HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
    String managerId = request.getParameter("managerId");
    if (managerId != null && !managerId.trim().isEmpty()) {
      OfficeWorker manager = service.getOne(Long.parseLong(managerId));
      value.setManager(manager);
    } else {
      value.setManager(null);
    }
		service.save(value);
		return SUCCESS;
  }

  @Override
	public String delete() {
		HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);

    try {
      service.deleteById(Long.parseLong(request.getParameter("id")));
    } catch (IllegalArgumentException ex) {
      request.getSession().setAttribute("error", ex.getMessage());
    }

		return SUCCESS;
	}
}
