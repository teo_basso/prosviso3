package com.example.teamorganizer.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.example.teamorganizer.model.Role;
import com.example.teamorganizer.model.Developer;
import com.example.teamorganizer.service.DeveloperService;
import com.opensymphony.xwork2.ActionContext;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Component;

@Component
public class DeveloperAction extends Action<Developer, DeveloperService> {

  private static final long serialVersionUID = 1L;

  private List<Role> roleList = new ArrayList<Role>();

  public DeveloperAction() {
    super(new Developer());
  }

  @Override
	public String saveOrUpdate() {
    HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
    String[] roleIds = request.getParameterValues("roleIds");
    Set<Role> roles = new HashSet<Role>();
    if (roleIds != null) {
      for (String roleId : roleIds) {
        if (!roleId.trim().isEmpty()) {
          roles.add(((DeveloperService) service).findRole(Long.parseLong(roleId)));
        }
      }
    }

    Developer value = getValue();
    value.setRoles(roles);
    service.saveAndFlush(value);
		return SUCCESS;
  }

  @Override
	public String list() {
    setList(service.findAll());
    setRoleList(((DeveloperService) service).findAllRoles());
		return SUCCESS;
	}

  @Override
	public String edit() {
		HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
		setValue(service.getOne(Long.parseLong(request.getParameter("id"))));
    setList(service.findAll());
    setRoleList(((DeveloperService) service).findAllRoles());
		return SUCCESS;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}
}
