package com.example.teamorganizer.action;

import javax.servlet.http.HttpServletRequest;

import com.example.teamorganizer.model.Role;
import com.example.teamorganizer.service.RoleService;
import com.opensymphony.xwork2.ActionContext;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Component;

@Component
public class RoleAction extends Action<Role, RoleService> {
  private static final long serialVersionUID = 1L;

  public RoleAction() {
    super(new Role());
  }

  @Override
	public String delete() {
		HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);

    try {
      service.deleteById(Long.parseLong(request.getParameter("id")));
    } catch (IllegalArgumentException ex) {
      request.getSession().setAttribute("error", ex.getMessage());
    }

		return SUCCESS;
	}
}
