package com.example.teamorganizer.action;

import com.example.teamorganizer.model.Office;
import com.example.teamorganizer.service.OfficeService;

import org.springframework.stereotype.Component;

@Component
public class OfficeAction extends Action<Office, OfficeService> {
  private static final long serialVersionUID = 1L;

  public OfficeAction() {
    super(new Office());
  }
}
