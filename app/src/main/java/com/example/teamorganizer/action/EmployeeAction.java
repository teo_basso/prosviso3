package com.example.teamorganizer.action;

import com.example.teamorganizer.model.Employee;
import com.example.teamorganizer.service.EmployeeService;

import org.springframework.stereotype.Component;

@Component
public class EmployeeAction extends Action<Employee, EmployeeService> {
  private static final long serialVersionUID = 1L;

  public EmployeeAction() {
    super(new Employee());
  }
}
