package com.example.teamorganizer.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.example.teamorganizer.service.ServiceFacade;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class Action<T, Service extends ServiceFacade<T, Long>> extends ActionSupport implements ModelDriven<T> {

  private static final long serialVersionUID = 1L;

  private List<T> list = new ArrayList<T>();
	private T value;

  @Autowired
  protected Service service;

  public Action(T value) {
    setValue(value);
  }

  @Override
	public T getModel() {
		return value;
	}

	public String saveOrUpdate() {
		service.saveAndFlush(value);
		return SUCCESS;
  }

	public String list() {
		setList(service.findAll());
		return SUCCESS;
	}

	public String delete() {
		HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
		service.deleteById(Long.parseLong(request.getParameter("id")));
		return SUCCESS;
	}

	public String edit() {
		HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
		setValue(service.getOne(Long.parseLong(request.getParameter("id"))));
		setList(service.findAll());
		return SUCCESS;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
}
