package com.example.teamorganizer.repository;

import java.util.List;
import java.util.Optional;

import com.example.teamorganizer.model.Employee;

public interface EmployeeRepositoryCustom<T extends Employee> {
  Optional<T> findBySerialNumber(String serialNumber);
  List<T> findByName(String name);
  List<T> findBySurname(String surname);
  List<T> findByNameAndSurname(String name, String surname);
}
