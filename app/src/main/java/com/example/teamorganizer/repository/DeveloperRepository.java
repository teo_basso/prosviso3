package com.example.teamorganizer.repository;

import com.example.teamorganizer.model.Developer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeveloperRepository extends JpaRepository<Developer, Long>, EmployeeRepositoryCustom<Developer>, DeveloperRepositoryCustom {
}
