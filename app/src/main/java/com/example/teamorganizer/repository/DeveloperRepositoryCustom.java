package com.example.teamorganizer.repository;

import com.example.teamorganizer.model.Role;
import com.example.teamorganizer.model.Developer;

import java.util.List;

public interface DeveloperRepositoryCustom {
  List<Developer> findByRoles(Role role);
  List<Developer> findByRoles_Id(Long roleId);
  List<Developer> findByRoles_Name(String roleName);
}
