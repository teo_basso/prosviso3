package com.example.teamorganizer.repository;

import com.example.teamorganizer.model.Role;

import java.util.List;
import java.util.Optional;

public interface RoleRepositoryCustom {
  Optional<Role> findByName(String name);
  List<Role> findByDescriptionContaining(String description);
}
