package com.example.teamorganizer.repository;

import com.example.teamorganizer.model.Office;
import java.util.List;

public interface OfficeRepositoryCustom {
  List<Office> findByCity(String city);
  List<Office> findByPostalCode(String postalCode);
  List<Office> findByCountryCode(String countryCode);
  List<Office> findByAddress(String address);
  List<Office> findByAddressContaining(String address);
  List<Office> findByDescriptionContaining(String description);
}
