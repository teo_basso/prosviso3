package com.example.teamorganizer.repository;

import java.util.Date;
import java.util.List;

import com.example.teamorganizer.model.Schedule;
import com.example.teamorganizer.model.Employee;
import com.example.teamorganizer.model.Project;

public interface ScheduleRepositoryCustom {
  List<Schedule> findByEmployee(Employee employee);
  List<Schedule> findByProject(Project project);
  List<Schedule> findByStartBetween(Date begin, Date end);
  List<Schedule> findByEndBetween(Date begin, Date end);
}
