package com.example.teamorganizer.repository;

import java.util.List;

import com.example.teamorganizer.model.OfficeWorker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfficeWorkerRepository extends JpaRepository<OfficeWorker, Long>, EmployeeRepositoryCustom<OfficeWorker> {
  List<OfficeWorker> findByManager(OfficeWorker manager);
  List<OfficeWorker> findByManagerId(Long id);
}
