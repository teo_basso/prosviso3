package com.example.teamorganizer.repository;

import com.example.teamorganizer.model.Project;
import com.example.teamorganizer.model.Office;

import java.util.List;
import java.util.Optional;

public interface ProjectRepositoryCustom {
  Optional<Project> findByNameAndVersion(String name, String version);
  List<Project> findByName(String name);
  List<Project> findByVersion(String model);
  List<Project> findByLocation(Office location);
  List<Project> findByVersionContaining(String model);
}
