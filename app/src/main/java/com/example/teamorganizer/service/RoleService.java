package com.example.teamorganizer.service;

import com.example.teamorganizer.model.Role;
import com.example.teamorganizer.repository.DeveloperRepository;
import com.example.teamorganizer.repository.RoleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("roleService")
public class RoleService extends ServiceFacadeImpl<Role, RoleRepository> {

  @Autowired
  private DeveloperRepository developerRepository;

  @Override
  public void deleteById(Long id) {
    if (!developerRepository.findByRoles_Id(id).isEmpty()) {
      throw new IllegalArgumentException("Cannot delete a role that is associated to a developer");
    }

    repository.deleteById(id);
    repository.flush();
  }

}
