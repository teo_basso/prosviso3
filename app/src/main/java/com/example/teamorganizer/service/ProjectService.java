package com.example.teamorganizer.service;

import java.util.List;

import com.example.teamorganizer.model.Office;
import com.example.teamorganizer.model.Project;
import com.example.teamorganizer.repository.OfficeRepository;
import com.example.teamorganizer.repository.ProjectRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("projectService")
public class ProjectService extends ServiceFacadeImpl<Project, ProjectRepository> {

  @Autowired
  private OfficeRepository officeRepository;

  public Office findOffice(Long id) {
    return officeRepository.getOne(id);
  }

  public List<Office> findAllOffices() {
    return officeRepository.findAll();
  }
}
