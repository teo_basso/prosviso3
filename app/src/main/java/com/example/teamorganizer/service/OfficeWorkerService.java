package com.example.teamorganizer.service;

import com.example.teamorganizer.model.OfficeWorker;
import com.example.teamorganizer.repository.OfficeWorkerRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("officeWorkerService")
public class OfficeWorkerService extends ServiceFacadeImpl<OfficeWorker, OfficeWorkerRepository> {

  @Override
  public void deleteById(Long id) {
    if (!repository.findByManagerId(id).isEmpty()) {
      throw new IllegalArgumentException("Cannot delete a manager");
    }

    repository.deleteById(id);
    repository.flush();
  }

}
