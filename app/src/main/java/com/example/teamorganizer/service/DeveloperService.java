package com.example.teamorganizer.service;

import java.util.List;

import com.example.teamorganizer.model.Role;
import com.example.teamorganizer.model.Developer;
import com.example.teamorganizer.repository.RoleRepository;
import com.example.teamorganizer.repository.DeveloperRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("developerService")
public class DeveloperService extends ServiceFacadeImpl<Developer, DeveloperRepository> {

  @Autowired
  private RoleRepository roleRepository;

  public Role findRole(Long id) {
    return roleRepository.getOne(id);
  }

  public List<Role> findAllRoles() {
    return roleRepository.findAll();
  }
}
