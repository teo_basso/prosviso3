package com.example.teamorganizer.service;

import java.util.List;

public interface ServiceFacade<T, Key> {
  public T save(T model);
  public T saveAndFlush(T model);
  public List<T> findAll();
  public void deleteById(Key id);
  public T getOne(Key id);
}
