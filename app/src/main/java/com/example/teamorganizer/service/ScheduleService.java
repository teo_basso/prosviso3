package com.example.teamorganizer.service;

import java.util.List;

import com.example.teamorganizer.model.Employee;
import com.example.teamorganizer.model.Project;
import com.example.teamorganizer.model.Schedule;
import com.example.teamorganizer.repository.EmployeeRepository;
import com.example.teamorganizer.repository.ProjectRepository;
import com.example.teamorganizer.repository.ScheduleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("scheduleService")
public class ScheduleService extends ServiceFacadeImpl<Schedule, ScheduleRepository> {

  @Autowired
  private EmployeeRepository employeeRepository;

  @Autowired
  private ProjectRepository projectRepository;

  public Employee findEmployee(Long id) {
    return employeeRepository.getOne(id);
  }

  public List<Employee> findAllEmployees() {
    return employeeRepository.findAll();
  }

  public Project findProject(Long id) {
    return projectRepository.getOne(id);
  }

  public List<Project> findAllProjects() {
    return projectRepository.findAll();
  }

}
