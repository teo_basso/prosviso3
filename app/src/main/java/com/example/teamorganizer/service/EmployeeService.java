package com.example.teamorganizer.service;

import java.util.List;

import com.example.teamorganizer.model.Developer;
import com.example.teamorganizer.model.Employee;
import com.example.teamorganizer.model.OfficeWorker;
import com.example.teamorganizer.repository.DeveloperRepository;
import com.example.teamorganizer.repository.EmployeeRepository;
import com.example.teamorganizer.repository.OfficeWorkerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("employeeService")
public class EmployeeService extends ServiceFacadeImpl<Employee, EmployeeRepository> {

  @Autowired
  private DeveloperRepository developerRepository;

  @Autowired
  private OfficeWorkerRepository officeWorkerRepository;

  @Override
  public List<Employee> findAll() {
    List<Employee> employees = repository.findAll();
    List<Developer> developers = developerRepository.findAll();
    List<OfficeWorker> officeWorkers = officeWorkerRepository.findAll();

    employees.removeIf(employee ->
      developers.stream().filter(dev -> dev.getId() == employee.getId()).toArray().length != 0 ||
      officeWorkers.stream().filter(worker -> worker.getId() == employee.getId()).toArray().length != 0
    );

    return employees;
  }
}
