package com.example.teamorganizer.service;

import com.example.teamorganizer.model.Office;
import com.example.teamorganizer.repository.OfficeRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("officeService")
public class OfficeService extends ServiceFacadeImpl<Office, OfficeRepository> {

}
