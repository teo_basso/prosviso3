package com.example.teamorganizer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class ServiceFacadeImpl<T, Repository extends JpaRepository<T, Long>> implements ServiceFacade<T, Long> {

    @Autowired
    protected Repository repository;

    public T save(T model) {
      return repository.save(model);
    }

    public T saveAndFlush(T model) {
      return repository.saveAndFlush(model);
    }

    public List<T> findAll() {
      return repository.findAll();
    }

    public void deleteById(Long id) {
      repository.deleteById(id);
      repository.flush();
    }

    public T getOne(Long id) {
      return repository.getOne(id);
    }
}
