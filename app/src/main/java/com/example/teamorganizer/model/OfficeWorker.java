package com.example.teamorganizer.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "officeWorkers")
@PrimaryKeyJoinColumn(name = "officeWorkerId")
public class OfficeWorker extends Employee implements Serializable {
    private static final long serialVersionUID = 1L;

    @ManyToOne(optional = true, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    private OfficeWorker manager;

    public OfficeWorker getManager() {
        return manager;
    }

    public void setManager(OfficeWorker manager) {
        this.manager = manager;
    }
}
