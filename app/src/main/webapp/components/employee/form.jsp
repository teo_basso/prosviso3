<div class="row">
  <div class="input-field col s4">
    <s:textfield
      id="serialNumber"
      name="serialNumber"
      class="validate"
      minlength="6"
      maxlength="6"
      required=""
    />
    <label class="active" for="serialNumber">Serial Number</label>
  </div>
  <div class="input-field col s4">
    <s:textfield
      id="name"
      name="name"
      class="validate"
      minlength="2"
      maxlength="50"
      required=""
    />
    <label class="active" for="name">Name</label>
  </div>
  <div class="input-field col s4">
    <s:textfield
      id="surname"
      name="surname"
      class="validate"
      minlength="2"
      maxlength="50"
      required=""
    />
    <label class="active" for="surname">Surname</label>
  </div>
</div>
