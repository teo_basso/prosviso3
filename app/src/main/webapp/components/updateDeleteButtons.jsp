<td>
  <s:url var="editURL" action="%{#editAction}">
    <s:param name="id" value="%{id}"></s:param>
  </s:url>
  <s:a href="%{editURL}"><i class="material-icons">create</i></s:a>
</td>
<td>
  <s:url var="deleteURL" action="%{#deleteAction}">
    <s:param name="id" value="%{id}"></s:param>
  </s:url>
  <s:a href="%{deleteURL}"><i class="material-icons">delete</i></s:a>
</td>
