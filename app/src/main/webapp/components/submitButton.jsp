<s:submit
  type="button"
  method="add"
  class="waves-effect waves-light btn"
  style="float: right"
>
  <s:if test="value.getId() == null">
    Add
    <i class="material-icons right">
      add
    </i>
  </s:if>
  <s:if test="value.getId() != null">
    Update
    <i class="material-icons right">
      create
    </i>
  </s:if>
</s:submit>
