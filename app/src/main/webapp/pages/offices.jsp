<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<s:set var="editAction" value="'editOffice'" />
<s:set var="deleteAction" value="'deleteOffice'" />
<s:set var="saveOrUpdateAction" value="'saveOrUpdateOffice'" />

<t:page title="Offices">
    <jsp:body>
      <div class="row">
        <s:form action="%{#saveOrUpdateAction}" class="col s12">
          <s:push value="value">
            <s:hidden name="id" />
            <div class="row">
              <div class="input-field col s3">
                <s:textfield
                  id="countryCode"
                  name="countryCode"
                  class="validate"
                  minlength="2"
                  maxlength="2"
                  required=""
                />
                <label class="active" for="countryCode">Country Code</label>
              </div>
              <div class="input-field col s3">
                <s:textfield
                  id="postalCode"
                  name="postalCode"
                  class="validate"
                  minlength="3"
                  maxlength="10"
                  required=""
                />
                <label class="active" for="postalCode">Postal Code</label>
              </div>
              <div class="input-field col s6">
                <s:textfield
                  id="city"
                  name="city"
                  class="validate"
                  minlength="2"
                  maxlength="50"
                  required=""
                />
                <label class="active" for="city">City</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s3">
                <s:textfield
                  id="address"
                  name="address"
                  class="validate"
                  required=""
                />
                <label class="active" for="name">Address</label>
              </div>
              <div class="input-field col s9">
                <s:textfield
                  id="description"
                  name="description"
                  class="validate"
                />
                <label class="active" for="description">Description</label>
              </div>
            </div>
            <%@include file="/components/submitButton.jsp"%>
          </s:push>
        </s:form>
      </div>

      <s:if test="list.size() > 0">
        <table class="striped responsive-table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Country Code</th>
              <th>Postal Code</th>
              <th>City</th>
              <th>Address</th>
              <th>Description</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <s:iterator value="list">
              <tr>
                <td><s:property value="id"/></td>
                <td><s:property value="countryCode"/></td>
                <td><s:property value="postalCode"/></td>
                <td><s:property value="city"/></td>
                <td><s:property value="address"/></td>
                <td><s:property value="description"/></td>
                <%@include file="/components/updateDeleteButtons.jsp"%>
              </tr>
            </s:iterator>
          </tbody>
        </table>
      </s:if>
      <%@include file="/components/emptyList.jsp"%>
    </jsp:body>
</t:page>
