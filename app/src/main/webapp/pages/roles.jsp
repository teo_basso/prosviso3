<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
  String error = (String)session.getAttribute("error");
  session.removeAttribute("error");
%>

<s:set var="editAction" value="'editRole'" />
<s:set var="deleteAction" value="'deleteRole'" />
<s:set var="saveOrUpdateAction" value="'saveOrUpdateRole'" />

<t:page title="Roles" toast='<%=error%>'>
    <jsp:body>
      <div class="row">
        <s:form action="%{#saveOrUpdateAction}" class="col s12">
          <s:push value="value">
            <s:hidden name="id" />
            <div class="row">
              <div class="input-field col s3">
                <s:textfield
                  id="name"
                  name="name"
                  class="validate"
                  minlength="2"
                  maxlength="50"
                  required=""
                />
                <label class="active" for="name">Name</label>
              </div>
              <div class="input-field col s9">
                <s:textfield
                  id="description"
                  name="description"
                  class="validate"
                />
                <label class="active" for="description">Description</label>
              </div>
            </div>
            <%@include file="/components/submitButton.jsp"%>
          </s:push>
        </s:form>
      </div>

      <s:if test="list.size() > 0">
        <table class="striped responsive-table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Description</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <s:iterator value="list">
              <tr>
                <td><s:property value="id"/></td>
                <td><s:property value="name"/></td>
                <td><s:property value="description"/></td>
                <%@include file="/components/updateDeleteButtons.jsp"%>
              </tr>
            </s:iterator>
          </tbody>
        </table>
      </s:if>
      <%@include file="/components/emptyList.jsp"%>
    </jsp:body>
</t:page>
