<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
  String error = (String)session.getAttribute("error");
  session.removeAttribute("error");
%>

<s:set var="editAction" value="'editOfficeWorker'" />
<s:set var="deleteAction" value="'deleteOfficeWorker'" />
<s:set var="saveOrUpdateAction" value="'saveOrUpdateOfficeWorker'" />

<t:page title="Office Workers" toast='<%=error%>'>
    <jsp:body>
      <div class="row">
        <s:form action="%{#saveOrUpdateAction}" class="col s12">
          <s:push value="value">
            <s:hidden name="id" />
            <%@include file="/components/employee/form.jsp"%>
            <div class="row">
              <div class="input-field col s4">
                <select name="managerId">
                  <option value="" ${value.id == null || value.manager == null ? "selected=''" : ""}>Choose</option>
                  <s:iterator var="officeWorker" value="list">
                    <s:if test="!#officeWorker.id.equals(value.id)">
                      <option value="${officeWorker.id}" ${value.manager != null && officeWorker.id.equals(value.manager.id) ?  "selected=''" : ""}><s:property value="serialNumber"/> - <s:property value="name"/> <s:property value="surname"/></option>
                    </s:if>
                  </s:iterator>
                </select>
                <label>Manager</label>
              </div>
            </div>
            <%@include file="/components/submitButton.jsp"%>
          </s:push>
        </s:form>
      </div>

      <s:if test="list.size() > 0">
        <table class="striped responsive-table">
          <thead>
            <tr>
              <%@include file="/components/employee/tableHeader.jsp"%>
              <th>Manager</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <s:iterator value="list">
              <tr>
                <%@include file="/components/employee/tableBody.jsp"%>
                <td><s:property value="manager.serialNumber"/> - <s:property value="manager.name"/> <s:property value="manager.surname"/></td>
                <%@include file="/components/updateDeleteButtons.jsp"%>
              </tr>
            </s:iterator>
          </tbody>
        </table>
      </s:if>
      <%@include file="/components/emptyList.jsp"%>
    </jsp:body>
</t:page>
