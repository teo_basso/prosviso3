<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<s:set var="editAction" value="'editDeveloper'" />
<s:set var="deleteAction" value="'deleteDeveloper'" />
<s:set var="saveOrUpdateAction" value="'saveOrUpdateDeveloper'" />

<t:page title="Developers">
    <jsp:body>
      <div class="row">
        <s:form action="%{#saveOrUpdateAction}" class="col s12">
          <s:push value="value">
            <s:hidden name="id" />
            <%@include file="/components/employee/form.jsp"%>
            <div class="row">
              <div class="input-field col s4">
                <select name="roleIds" multiple="">
                  <option value="" ${value.id == null || value.roles == null || value.roles.isEmpty() ? "selected=''" : ""}>Choose</option>
                  <s:iterator var="role" value="roleList">
                    <option value="${role.id}" ${value.roles != null && value.roles.contains(role) ?  "selected=''" : ""}><s:property value="name"/></option>
                  </s:iterator>
                </select>
                <label>Roles</label>
              </div>
            </div>
            <%@include file="/components/submitButton.jsp"%>
          </s:push>
        </s:form>
      </div>

      <s:if test="list.size() > 0">
        <table class="striped responsive-table">
          <thead>
            <tr>
              <%@include file="/components/employee/tableHeader.jsp"%>
              <th>Roles</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <s:iterator var="developer" value="list">
              <tr>
                <%@include file="/components/employee/tableBody.jsp"%>
                <td>
                  <s:if test="developer.roles == null || developer.roles.isEmpty()">
                    -
                  </s:if>
                  <s:iterator status="status" var="role" value="#developer.roles"><s:if test="#status.index > 0">, </s:if><s:property value="name" /></s:iterator>
                </td>
                <%@include file="/components/updateDeleteButtons.jsp"%>
              </tr>
            </s:iterator>
          </tbody>
        </table>
      </s:if>
      <%@include file="/components/emptyList.jsp"%>
    </jsp:body>
</t:page>
