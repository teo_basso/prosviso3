<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:page title="Home">
    <jsp:body>
      <h4>Welcome into the Team Organizer App!</h4>
      <p>Open the menu clicking on the icon in the app bar and start scheduling the work of your business</p>
    </jsp:body>
</t:page>
