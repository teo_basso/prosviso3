<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<s:set var="editAction" value="'editEmployee'" />
<s:set var="deleteAction" value="'deleteEmployee'" />
<s:set var="saveOrUpdateAction" value="'saveOrUpdateEmployee'" />

<t:page title="Employees">
    <jsp:body>
      <div class="row">
        <s:form action="%{#saveOrUpdateAction}" class="col s12">
          <s:push value="value">
            <s:hidden name="id" />
            <%@include file="/components/employee/form.jsp"%>
            <%@include file="/components/submitButton.jsp"%>
          </s:push>
        </s:form>
      </div>

      <s:if test="list.size() > 0">
        <table class="striped responsive-table">
          <thead>
            <tr>
              <%@include file="/components/employee/tableHeader.jsp"%>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <s:iterator value="list">
              <tr>
                <%@include file="/components/employee/tableBody.jsp"%>
                <%@include file="/components/updateDeleteButtons.jsp"%>
              </tr>
            </s:iterator>
          </tbody>
        </table>
      </s:if>
      <%@include file="/components/emptyList.jsp"%>
    </jsp:body>
</t:page>
