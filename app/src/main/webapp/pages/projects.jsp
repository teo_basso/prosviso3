<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<s:set var="editAction" value="'editProject'" />
<s:set var="deleteAction" value="'deleteProject'" />
<s:set var="saveOrUpdateAction" value="'saveOrUpdateProject'" />

<t:page title="Projects">
    <jsp:body>
      <div class="row">
        <s:form action="%{#saveOrUpdateAction}" class="col s12">
          <s:push value="value">
            <s:hidden name="id" />
            <div class="row">
              <div class="input-field col s4">
                <s:textfield
                  id="name"
                  name="name"
                  class="validate"
                  minlength="2"
                  maxlength="50"
                  required=""
                />
                <label class="active" for="name">Name</label>
              </div>
              <div class="input-field col s4">
                <s:textfield
                  id="version"
                  name="version"
                  class="validate"
                  minlength="1"
                  maxlength="20"
                />
                <label class="active" for="version">Version</label>
              </div>
              <div class="input-field col s4">
                <select name="locationId" required="">
                  <s:iterator var="office" value="offices">
                    <option value="${office.id}" ${office.id.equals(value.location.id) ?  "selected=''" : ""}><s:property value="city"/> - <s:property value="address"/></option>
                  </s:iterator>
                </select>
                <label>Location</label>
              </div>
            </div>
            <%@include file="/components/submitButton.jsp"%>
          </s:push>
        </s:form>
      </div>

      <s:if test="list.size() > 0">
        <table class="striped responsive-table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Version</th>
              <th>Location</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <s:iterator value="list">
              <tr>
                <td><s:property value="id"/></td>
                <td><s:property value="name"/></td>
                <td><s:property value="version"/></td>
                <td><s:property value="location.city"/> - <s:property value="location.address"/></td>
                <%@include file="/components/updateDeleteButtons.jsp"%>
              </tr>
            </s:iterator>
          </tbody>
        </table>
      </s:if>
      <%@include file="/components/emptyList.jsp"%>
    </jsp:body>
</t:page>
