<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:page title="">
    <jsp:body>
        <h4>We are sorry, something went wrong!</h4>
        <p>Please check that all the values that you've inserted are correct</p>
        <p>Return to the <a href="/" class="waves-effect">Home page</a></p>
    </jsp:body>
</t:page>
