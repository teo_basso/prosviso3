<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<s:set var="editAction" value="'editSchedule'" />
<s:set var="deleteAction" value="'deleteSchedule'" />
<s:set var="saveOrUpdateAction" value="'saveOrUpdateSchedule'" />

<t:page title="Schedules">
    <jsp:body>
      <div class="row">
        <s:form action="%{#saveOrUpdateAction}" class="col s12">
          <s:push value="value">
            <s:hidden name="id" />
            <div class="row">
              <div class="input-field col s3">
                <input id="start" name="start" class="c-datepicker-input" value='<s:date name="start" format="dd/MM/yyyy HH:mm" />' required="" />
                <label class="active" for="start">Start</label>
              </div>
              <div class="input-field col s3">
                <input id="end" name="end" class="c-datepicker-input" value='<s:date name="end" format="dd/MM/yyyy HH:mm" />' required="" />
                <label class="active" for="end">End</label>
              </div>
              <div class="input-field col s3">
                <select name="employeeId" required="">
                  <s:iterator var="currentEmployee" value="employees">
                    <option value="${currentEmployee.id}" ${value.employee != null && currentEmployee.id.equals(value.employee.id) ?  "selected=''" : ""}><s:property value="serialNumber"/> - <s:property value="name"/> <s:property value="surname"/></option>
                  </s:iterator>
                </select>
                <label>Employee</label>
              </div>
              <div class="input-field col s3">
                <select name="projectId">
                  <option value="" ${value.id == null ? "selected=''" : ""}>Choose</option>
                  <s:iterator var="project" value="projects">
                    <option value="${project.id}" ${value.project != null && project.id.equals(value.project.id) ?  "selected=''" : ""}><s:property value="name"/> - <s:property value="version"/></option>
                  </s:iterator>
                </select>
                <label>Project</label>
              </div>
            </div>
            <%@include file="/components/submitButton.jsp"%>
          </s:push>
        </s:form>
      </div>

      <s:if test="list.size() > 0">
        <table class="striped responsive-table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Start</th>
              <th>End</th>
              <th>Employee</th>
              <th>Project</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <s:iterator value="list">
              <tr>
                <td><s:property value="id"/></td>
                <td><s:date name="start" format="dd/MM/yyyy HH:mm" /></td>
                <td><s:date name="end" format="dd/MM/yyyy HH:mm" /></td>
                <td><s:property value="employee.serialNumber"/> - <s:property value="employee.name"/> <s:property value="employee.surname"/></td>
                <td><s:property value="project.name"/> - <s:property value="project.version"/></td>
                <%@include file="/components/updateDeleteButtons.jsp"%>
              </tr>
            </s:iterator>
          </tbody>
        </table>
      </s:if>
      <%@include file="/components/emptyList.jsp"%>
    </jsp:body>
</t:page>
